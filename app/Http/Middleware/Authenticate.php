<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function redirectTo($request)
    {
        if(strpos($_SERVER['REQUEST_URI'], 'admin') !== false){
            return redirect('/admin/login');
        }else{
            if(strpos($_SERVER['REQUEST_URI'], 'worker') !== false){
                return redirect('/worker/login');
            }else{
                return route('pages.login');
            }
        }

    }
}
