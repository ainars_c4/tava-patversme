<?php

namespace App\Http\Controllers;

use App\Models\ActionLog;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Models\Shelter;
use App\Models\Worker;
use App\Models\Contactinfo;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Arr;

class ShelterController extends Controller
{

    private function checkIfAdmin()
    {
        if (Auth::guard('admin')->check()) {
            return true;
        } else {
            return false;
        }
    }

    private function addContactInfo($id, $data)
    {
        if (Arr::exists($data, 'email') || Arr::exists($data, 'phone')) {
            $contact = new Contactinfo;
            $contact->shelter_id = $id;
            if (Arr::exists($data, 'email')) {
                $contact->email = $data["email"];
            }
            if (Arr::exists($data, 'phone')) {
                $contact->phone = $data["phone"];
            }
            $contact->save();
        }
    }

    public function showEditShelterForm($id)
    {
//        if (!$this->checkIfAdmin()) {
//            redirect('/admin');
//        }

        //Finds shelter
        $shelter = DB::table('shelters')->where([
            ['id', '=', $id],
        ])->get();

        //Finds a worker
        $worker = DB::table('shelter_workers')->where([
            ['shelter_id', '=', $id],
            ['admin', '=', true],
        ])->get();

        //Finds contact info
        $contactinfo = DB::table('contact_info')->where('shelter_id', '=', $id)->get();

        return view('pages.admin.editshelter')->with('shelter', $shelter[0])->with('worker', $worker[0])->with('contactinfo', $contactinfo);
    }

    public function editShelter($id, Request $request)
    {
//        if (!$this->checkIfAdmin()) {
//            redirect('/admin');
//        }
        //Input
        $data = Input::except(array('_token'));

        //Validation rules
        $rule = array(
            'name' => 'required',
            'city' => 'required',
            'street' => 'required',
            'street_nr' => 'required',
            'picture' => 'required',
        );

        //Validation messages
        $messages = array(
            'required' => 'Netika aizpildīti visi lauki!',
        );

        //Validates
        $validator = Validator::make($data, $rule, $messages);

        //Updates data
        DB::table('shelters')
            ->where('id', $id)
            ->update([
                'name' => $data['name'],
                'city' => $data['city'],
                'street' => $data['street'],
                'street_nr' => $data['street_nr'],
            ]);

        //File managment

        if ($request->hasFile('picture')) {

            //Removes old file
            Storage::delete('public/stored_shelters/' . $id . '.jpg');

            //Adds new file
            $file = $request->file('picture')->storeAs(
                'public/stored_shelters', $id . '.jpg'
            );
        }


        return redirect('/admin')->with('success', 'Patversme nr. ' . $id . ' tika veiksmīgi rediģēta!');
    }

    public function addContactFromForm($id)
    {
        if (!$this->checkIfAdmin()) {
            redirect('/admin');
        }

        //Input
        $data = Input::except(array('_token'));

        //Validation rules
        if (is_null($data["Email"]) && is_null($data["phone"])) {
            Session::flash('message-contact-info-added', 'Lūdzu ievadiet e-pastu vai tālruņa nr.!');
            return redirect('/admin/edit-shelter/' . $id);
        }
        if (is_null($data["email"])) {
            $rule = array();
        } else {
            $rule = array(
                'email' => 'email'
            );
        }


        //Validation messages
        $messages = array(
            'required' => 'Netika aizpildīti visi lauki!',
        );

        //Validates
        $validator = Validator::make($data, $rule, $messages);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput(Input::all());
        }

        $this->addContactInfo($id, $data);

        Session::flash('message-contact-info-added', 'Kontaktinformācija veiksmīgi pievienota!');
        return redirect('/admin/edit-shelter/' . $id);
    }

    public function deleteContact($id)
    {
//        if (!$this->checkIfAdmin()) {
//            redirect('/admin');
//        }

        //Deletes the contact information
        DB::table('contact_info')->where('id', '=', $id)->delete();

        //Paziņojums par veikto darbību
        Session::flash('message-contact-info-deleted', "Informācija veiksmīgi dzēsta!");
        return redirect()->back();
    }

    public function editContact($id)
    {
        if (!$this->checkIfAdmin()) {
            redirect('/admin');
        }

        //Input
        $data = Input::except(array('_token'));

        //Validation rules
        if (is_null($data["email"])) {
            $rule = array(
                'phone' => 'required'
            );
        } else {
            $rule = array(
                'email' => 'email'
            );
        }

        //Validation messages
        $messages = array(
            'required' => 'Lūdzu ievadiet e-pastu vai tālruņa nr.!',
        );

        //Validates
        $validator = Validator::make($data, $rule, $messages);

        if ($validator->fails()) {
            Session::flash('message-contact-info-needed', 'Lūdzu ievadiet e-pastu vai tālruņa nr.!');
            return redirect()->back()
                ->withErrors($validator)
                ->withInput(Input::all());
        }

        //Saves the new data
        DB::table('contact_info')->where('id', '=', $id)->update();

        Session::flash('message-contact-info-updated', 'Kontaktinformācija veiksmīgi atjaunota!');
        return redirect('/admin/edit-shelter/' . $id);
    }

    public function getShelterNames()
    {
        $data = Shelter::all(['id', 'name AS text']);
        return ($data);
    }


}
