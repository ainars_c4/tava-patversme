<?php

namespace App\Http\Controllers;

use App\Traits\ActionLogger;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Animal;
use Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use App\Models\Disease;
use App\Models\AnimalHealthHistory;
use App\Models\Application;
use App\Models\Category;


class AnimalController extends Controller
{

    use ActionLogger;

    /**
     * @param $WorkerID
     * @param $AnimalID
     * @return bool
     */
    private function isOfSameShelter($WorkerID, $AnimalID)
    {

        //Gets worker Shelter ID
        $WorkerShelterID = DB::table('shelter_workers')->select('shelter_id')->where('id', '=', $WorkerID)->get()[0]->shelter_id;

        //Gets animal Shelter ID
        $AnimalShelterID = DB::table('animals')->select('shelter_id')->where('id', '=', $AnimalID)->get()[0]->shelter_id;

        if ($AnimalShelterID == $WorkerShelterID) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * @return bool
     */
    private function isWorker()
    {
        if (Auth::guard('worker')->check()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function createAnimal(Request $request)
    {
        if (!$this->isWorker()) {
            return view('pages.worker.login');
        }

        //Input
        $data = Input::except(array('_token'));

        //Validation rules
        $rule = array(
            'mame' => 'required',
            'category_id' => 'required',
            'desc' => 'required',
            'picture' => 'required',
            'shelter_id' => 'required',
            'birth' => 'required'
        );

        //Validation messages
        $messages = array(
            'required' => 'Netika aizpildīti visi lauki!',
        );

        //Validates
        $validator = Validator::make($data, $rule, $messages);

        //Creates shelter
        $animal = new Animal;
        $animal->name = $data['name'];
        $animal->category_id = $data['category_id'];
        $animal->desc = $data['desc'];
        $animal->picture = $data['picture'];
        $animal->shelter_id = $data['shelter_id'];
        $animal->birth = $data['birth'];

        //Saves shelter
        $animal->save();

        //Creates a log
        $user = Auth::guard('worker')->user();
        $action = $user->firstname." ".$user->lastname." ievietoja jaunu dzīvnieku (".$animal->name.")";
        $this->createLog($user->firstname." ".$user->lastname." (".$user->email.")", 3000, $action, $user->shelter_id);

        //Sets name for file before storing it
        $fileid = DB::table('animals')->where([
            ['name', '=', $data['name']],
            ['desc', '=', $data['desc']],
            ['picture', '=', $data['picture']],
        ])->get();

        //Saves file
        $file = $request->file('picture')->storeAs(
            'public/stored_animals', $fileid[0]->id . '.jpg'
        );

        //Sets correct filename when saving database
        DB::table('animals')
            ->where('ID', $fileid[0]->id)
            ->update(['picture' => 'stored_animals/' . $fileid[0]->id . '.jpg']);

        Session::flash('add-animal-success', "Dzīvnieks veiksmīgi pievienots!");
        return redirect()->back();
    }

    public function getEditAnimalView($id)
    {

        //Checks if worker is working with animals in his shelter
        if (!$this->isOfSameShelter(Auth::guard('worker')->user()->id, $id)) {
            return redirect('/worker');
        }

        if (!$this->isWorker()) {
            return view('pages.worker.login');
        }

        //Finds animal
        $animal = DB::table('animals')->where([
            ['id', '=', $id],
        ])->get();

        //Gets the categories
        $categories = DB::table('categories')->get();

        //Gets the diseases
        $diseases = DB::table('diseases')->get();

        //Gets the animals diseases
        $animal_diseases = DB::table('animal_health_history')
            ->join('diseases', 'diseases.id', '=', 'animal_health_history.sicknessid')
            ->select('animal_health_history.*', 'diseases.name')
            ->where('animalid', '=', $animal[0]->id)
            ->get();

        //Gets the shelter
        $shelter = DB::table('shelters')->where('id', '=', Auth::guard('worker')->user()->shelter_id)->get();

        //Gets the worker
        $worker = Auth::guard('worker')->user();


        return view('pages.worker.animal.animaledit')
            ->with('diseases', $diseases)
            ->with('animal_diseases', $animal_diseases)
            ->with('worker', $worker)
            ->with('shelter', $shelter[0])
            ->with('categories', $categories)
            ->with('animal', $animal[0]);

    }

    public function editAnimal($id, Request $request)
    {
        //Cheks if this is worker
        if (!$this->isWorker()) {
            return view('pages.worker.login');
        }

        //Checks if worker is working with animals in his shelter
        if (!$this->isOfSameShelter(Auth::guard('worker')->user()->id, $id)) {
            return redirect('/worker');
        }

        //Input
        $data = Input::except(array('_token'));

        //Validation rules
        $rule = array(
            'name' => 'required',
            'category_id' => 'required',
            'desc' => 'required',
            'picture' => 'required',
            'birth' => 'required'
        );

        //Validation messages
        $messages = array(
            'required' => 'Netika aizpildīti visi lauki!',
        );

        //Validates
        $validator = Validator::make($data, $rule, $messages);

        //Updates data
        DB::table('animals')
            ->where('id', $id)
            ->update([
                'name' => $data['name'],
                'category_id' => $data['category_id'],
                'desc' => $data['desc'],
                'birth' => $data['birth'],
                'picture' => 'stored_animals/'.$id.'.jpg',
            ]);

        //Creates a log
        $animal = Animal::find($id);
        $user = Auth::guard('worker')->user();
        $action = $user->firstname." ".$user->lastname." atjaunoja dzīvnieka ".$animal->name." datus";
        $this->createLog($user->firstname." ".$user->lastname." (".$user->email.")", 3001, $action, $user->shelter_id);

        //File managment

        if ($request->hasFile('picture')) {

            //Removes old file
            Storage::delete('public/stored_animals/' . $id . '.jpg');

            //Adds new file
            $file = $request->file('picture')->storeAs(
                'public/stored_animals', $id . '.jpg'
            );
        }


        return redirect()->back()->with('edit-animal-success', 'Dzīvnieks nr. ' . $id . ' tika veiksmīgi rediģēts!');
    }

    public function deleteAnimal($id)
    {
        //Cheks if this is worker
        if (!$this->isWorker()) {
            return view('pages.worker.login');
        }

        //Checks if worker is working with animals in his shelter
        if (!$this->isOfSameShelter(Auth::guard('worker')->user()->id, $id)) {
            return redirect('/worker');
        }

        //Deletes animal picture
        Storage::delete('public/stored_animals/' . $id . '.jpg');

        //Creates a log
        $animal = Animal::find($id);
        $user = Auth::guard('worker')->user();
        $action = $user->firstname." ".$user->lastname." izdzēsa dzīvnieku ".$animal->name;
        $this->createLog($user->firstname." ".$user->lastname." (".$user->email.")", 3002, $action, $user->shelter_id);

        //Deletes the animal
        Animal::destroy($id);

        Session::flash('message-delete-animal-success', 'Dzīvnieks nr. ' . $id . ' tika veiksmīgi dzēsts!');
        return redirect()->back();

    }

    public function addNewExistingSickness($id)
    {

        //Checks if worker is working with animals in his shelter
        if (!$this->isOfSameShelter(Auth::guard('worker')->user()->id, $id)) {
            return redirect('/worker');
        }

        if (!$this->isWorker()) {
            return view('pages.worker.login');
        }

        //Input
        $data = Input::except(array('_token'));

        //Validation rules
        $rule = array(
            'sicknessid' => 'required',
            'desc' => 'required',
            'begindate' => 'required'
        );

        //Validation messages
        $messages = array(
            'required' => 'Netika aizpildīti visi lauki!',
        );

        //Validates
        $validator = Validator::make($data, $rule, $messages);


        //Makes the sickness record
        $health = new AnimalHealthHistory;
        $health->animalid = $id;
        $health->sicknessid = $data["sicknessid"];
        $health->desc = $data["desc"];
        $health->begindate = $data["begindate"];
        $health->save();

        //Creates a log
        $animal = Animal::find($id);
        $user = Auth::guard('worker')->user();
        $action = $user->firstname." ".$user->lastname." pievienoja jaunu ierakstu ".$animal->name." veselības vēsturē (".Disease::find($data["sicknessid"])->name.")";
        $this->createLog($user->firstname." ".$user->lastname." (".$user->email.")", 3004, $action, $user->shelter_id);

        //Goes back
        Session::flash('add-sickness-success', "Slimība pievienota");
        return redirect()->back();
    }

    public function getEditAnimalSicknessForm($AnimalID, $SicknessID, $BeginDate)
    {

        //Checks if worker is working with animals in his shelter
        if (!$this->isOfSameShelter(Auth::guard('worker')->user()->id, $AnimalID)) {
            return redirect('/worker');
        }

        if (!$this->isWorker()) {
            return view('pages.worker.login');
        }

        //Gets the diseases
        $diseases = DB::table('diseases')->get();

        //Gets the animals disease
        $animal_disease = DB::table('animal_health_history')
            ->join('diseases', 'diseases.id', '=', 'animal_health_history.sicknessid')
            ->select('animal_health_history.*', 'diseases.name')
            ->where('animalid', '=', $AnimalID)
            ->where('sicknessid', '=', $SicknessID)
            ->where('begindate', '=', $BeginDate)
            ->get();

        //Gets the shelter
        $shelter = DB::table('shelters')->where('id', '=', Auth::guard('worker')->user()->shelter_id)->get();

        //Gets the worker
        $worker = Auth::guard('worker')->user();

        return view('pages.worker.animal.sicknessedit')
            ->with('diseases', $diseases)
            ->with('animal_disease', $animal_disease[0])
            ->with('worker', $worker)
            ->with('shelter', $shelter[0]);

    }

    public function editAnimalSickness($AnimalID, $SicknessID, $BeginDate)
    {

        //Checks if worker is working with animals in his shelter
        if (!$this->isOfSameShelter(Auth::guard('worker')->user()->id, $AnimalID)) {
            return redirect('/worker');
        }

        if (!$this->isWorker()) {
            return view('pages.worker.login');
        }

        //Input
        $data = Input::except(array('_token'));

        //Validation rules
        $rule = array(
            'desc' => 'required',
            'begindate' => 'required'
        );

        //Validation messages
        $messages = array(
            'required' => 'Netika aizpildīti visi lauki!',
        );

        //Validates
        $validator = Validator::make($data, $rule, $messages);

        //Creates a log
        $animal = Animal::find($AnimalID);
        $user = Auth::guard('worker')->user();
        $action = $user->firstname." ".$user->lastname." atjaunoja ierakstu ".$animal->name." veselības vēsturē (".Disease::find($data[$SicknessID])->name.")";
        $this->createLog($user->firstname." ".$user->lastname." (".$user->email.")", 3005, $action, $user->shelter_id);

        //Saves new data
        DB::table('animal_health_history')
            ->where('animalid', $AnimalID)
            ->where('sicknessid', $SicknessID)
            ->where('begindate', $BeginDate)
            ->update([
                'desc' => $data["desc"]
            ]);


        //Goes back
        Session::flash('edit-animal-sickness-success', "Slimība rediģēta!");
        return redirect('/worker/edit-animal/' . $AnimalID);
    }

    public function deleteAnimalSickness($AnimalID, $SicknessID, $BeginDate)
    {

        //Checks if worker is working with animals in his shelter
        if (!$this->isOfSameShelter(Auth::guard('worker')->user()->id, $AnimalID)) {
            return redirect('/worker');
        }

        //Checks if user is a worker
        if (!$this->isWorker()) {
            return view('pages.worker.login');
        }

        //Deletes the sickness
        DB::table('animal_health_history')
            ->where('animalid', $AnimalID)
            ->where('sicknessid', $SicknessID)
            ->where('begindate', $BeginDate)
            ->delete();

        //Creates a log
        $animal = Animal::find($AnimalID);
        $user = Auth::guard('worker')->user();
        $action = $user->firstname." ".$user->lastname." izdzēsa ierakstu ".$animal->name." veselības vēsturē (".Disease::find($SicknessID)->name.")";
        $this->createLog($user->firstname." ".$user->lastname." (".$user->email.")", 3006, $action, $user->shelter_id);

        //Goes back
        Session::flash('delete-animal-sickness-success', "Slimība dzēsta!");
        return redirect('/worker/edit-animal/' . $AnimalID);

    }

    public function getViewAnimalPage($id)
    {
        $animal = DB::table('animals')
            ->select('animals.name AS name', 'shelters.name AS sheltername', 'animals.desc', 'animals.picture', 'animals.birth', 'shelters.city', 'shelters.street', 'shelters.street_nr', 'shelters.picture AS shelterpic', 'animals.id', 'animals.adopted', 'shelters.id AS shelter_id')
            ->join('shelters', 'animals.shelter_id', '=', 'shelters.id')
            ->where('animals.ID', '=', $id)->first();

        $contactinfo = DB::table("contact_info")->where('shelter_id', '=', $animal->shelter_id)->get();

        //Checks if animal is adopted and if the user has adopted this animal
        if ($animal->adopted == 1) {
            $adoptee = DB::table('applications')
                ->select('userid')
                ->where('animalid', '=', $id)
                ->where('status', '=', 3)
                ->first()->UserID;

            if ($adoptee != Auth::guard('web')->id()) {
                return redirect('/');
            }

        }

        $health_history = DB::table('animal_health_history')->join('diseases', 'animal_health_history.sicknessid', 'diseases.id')->where('animalid', '=', $id)->get();

        $has_applied = (Count(DB::table('applications')->where('animalid', '=', $id)->where('userid', '=', Auth::guard('web')->id())->where('status', '!=', 2)->get()));

        return view('pages.viewanimal')->with('animal', $animal)->with('health_history', $health_history)->with('has_applied', $has_applied)->with('contactinfo', $contactinfo);
    }

    public function getAdoptPage($id)
    {
        if (Auth::guard('web')->check()) {
            $animal = DB::table('animals')->where('ID', '=', $id)->first();
            $allAnimalDiseases = DB::table('animal_health_history')
                ->select('animal_health_history.desc', 'animal_health_history.begindate' ,'diseases.name')
                ->join('diseases', 'animal_health_history.sicknessid', '=', 'diseases.id')
                ->where('animal_health_history.animalid', '=', $id)
                ->get();
            return view('pages.adopt')->with('animal', $animal)->with('allAnimalDiseases', $allAnimalDiseases);
        } else {
            return redirect()->back();
        }
    }

    public function makeApplication($id, Request $request)
    {
        if (Auth::guard('web')->check()) {

            //Checks if the animal is already applied for
            $has_applied = (Count(DB::table('applications')->where('animalid', '=', $id)->where('userid', '=', Auth::guard('web')->id())->where('status', '!=', 2)->get()));
            if ($has_applied) {
                return redirect()->back();
            }

            //Input
            $data = Input::except(array('_token'));

            //Validation rules
            $rule = array(
                'answer1' => 'required',
                'answer2' => 'required',
                'picture' => 'required',
            );

            //Validation messages
            $messages = array(
                'required' => 'Netika aizpildīti visi lauki!',
            );

            //Validates
            $validator = Validator::make($data, $rule, $messages);

            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput(Input::all());
            }

            //Creates application
            $application = new Application;
            $application->UserID = Auth::guard('web')->id();
            $application->AnimalID = $id;
            $application->Answer1 = $data["answer1"];
            $application->Answer2 = $data["answer2"];
            $application->Picture = $data["picture"];
            $application->Status = 1;

            //Saves application
            $application->save();

            //Creates a log
            $animal = Animal::find($id);
            $user = Auth::guard('web')->user();
            $action = $user->firstname." ".$user->lastname." aizpildīja pieteikuma anketu dzīvniekam ".$animal->name;
            $this->createLog($user->firstname." ".$user->lastname." (".$user->email.")", 1004, $action, $animal->shelter_id);

            //Sets name for file before storing it
            $fileid = DB::table('applications')->where('id', '=', $application->id)->get();

            //Saves file
            $file = $request->file('picture')->storeAs(
                'public/stored_habitats', $fileid[0]->id . '.jpg'
            );

            //Sets correct filename when saving database
            DB::table('applications')
                ->where('id', $fileid[0]->id)
                ->update(['picture' => 'stored_habitats/' . $fileid[0]->id . '.jpg']);

            return view('pages.applicationsuccess');

        } else {
            return redirect()->back();
        }
    }

    public function cancelAdoption($id)
    {

        if (!$this->isWorker()) {
            return view('pages.worker.login');
        }

        //Checks if worker is working with animals in his shelter
        if (!$this->isOfSameShelter(Auth::guard('worker')->user()->id, $id)) {
            return redirect('/worker');
        }

        //Changes the animal status to not adopted
        DB::table('animals')->where('id', '=', $id)->update(['adopted' => 0]);

        //Creates a log
        $animal = Animal::find($id);
        $user = Auth::guard('worker')->user();
        $action = $user->firstname." ".$user->lastname." noņēma adoptēta dzīvnieka statusu dzīvniekam ".$animal->name;
        $this->createLog($user->firstname." ".$user->lastname." (".$user->email.")", 3007, $action, $user->shelter_id);

        //Update the succesful application for animal adoption
        DB::table('applications')
            ->where('animalid', '=', $id)
            ->where('status', '=', 3)
            ->update(['status' => 2]);

        //Goes back
        Session::flash('message-adoption-revoked', 'Dzīvnieks veiksmīgi ielikts atpakaļ adopcijā!');
        return redirect()->route('worker.dashboard');
    }

    public function getAnimalNames()
    {
        $data = Animal::all(['id', 'name AS text']);
        return ($data);
    }


}
