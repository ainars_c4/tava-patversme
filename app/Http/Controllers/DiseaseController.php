<?php

namespace App\Http\Controllers;

use App\Models\Disease;


class DiseaseController extends Controller
{

    public function getDiseaseNames()
    {
        $data = Disease::all(['id', 'name AS text']);
        return ($data);
    }

}
