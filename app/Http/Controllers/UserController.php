<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Traits\ActionLogger;
use Illuminate\Http\Request;
use Auth;
use Route;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Mail;

class UserController extends Controller
{

    use ActionLogger;

    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    }

    public function getEditPage()
    {
        $applications = DB::table('applications')
            ->select('applications.*', 'animals.name')
            ->join('animals', 'animals.id', '=', 'applications.animalid')
            ->where('userid', '=', Auth::guard('web')->id())->get();
        return view('pages.edituser')->with('applications', $applications);
    }

    public function edit()
    {
        //Input
        $data = Input::except(array('_token'));

        //Validation rules
        $rule = array(
            'firstname' => 'required',
            'lastname' => 'required',
            'phone' => 'required',
            'city' => 'required',
            'street' => 'required',
            'street_nr' => 'required'
        );

        //Validation messages
        $messages = array(
            'required' => 'Netika aizpildīti visi lauki!',
        );

        //Validates
        $validator = Validator::make($data, $rule, $messages);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput(Input::all());
        }

        $user = Auth::guard('web')->user();

        //Creates a log
        $action = $user->firstname." ".$user->lastname." atjaunoja savus datus uz ".$data["firstname"]." ".$data["lastname"];
        $this->createLog($user->firstname." ".$user->lastname." (".$user->email.")", 1001, $action);
        //Saves new data
        $user->Firstname = $data["firstname"];
        $user->Lastname = $data["lastname"];
        $user->Phone = $data["phone"];
        $user->City = $data["city"];
        $user->Street = $data["street"];
        $user->Street_nr = $data["street_nr"];
        $user->save();

        //Goes back
        Session::flash('edit-success', "Jūsu dati veiksmīgi rediģēti!");
        return redirect('/profile');
    }

    public function changePassword()
    {
        //Input
        $data = Input::except(array('_token'));

        //Validation rules
        $rule = array(
            'password' => 'required'
        );

        //Validation messages
        $messages = array(
            'required' => 'Netika aizpildīti visi lauki!',
        );

        //Validates
        $validator = Validator::make($data, $rule, $messages);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput(Input::all());
        }

        $user = Auth::guard('web')->user();

        //Generates token
        $token = bcrypt($data['password']);
        $user->token = $token;
        $user->save();

        //Sends verification email
        $to_name = $user->firstname . ' ' . $user->lastname;
        $to_email = $user->email;
        $mail_data = ['token' => $token];
        Mail::send('emails.verifychangepassword', $mail_data, function ($message) use ($to_name, $to_email) {
            $message->to($to_email, $to_name)
                ->subject('Profila paroles atjaunošana');
            $message->from('tavapat@admin.lv', 'TavaPatversme Administrators');
        });

        return redirect()->route('auth.show_verify_change_password');
    }

    public function getVerifyChangePasswordPage()
    {
        return view('pages.verify-change-password');
    }

    public function verifyChangePassword()
    {
        $token = Input::get('token');

        $user = User::where('token', '=', $token)->first();

        if ($user) {
            $user->password = $token;
            $user->save();

            //Creates a log
            $action = $user->firstname." ".$user->lastname." atjaunoja savu paroli";
            $this->createLog($user->firstname." ".$user->lastname." (".$user->email.")", 1003, $action);

            Session::flash('message-password-change-success', "Jūsu parole veiksmīgi samainīta!");
            return redirect('/');
        }

    }

    public function getRestorePasswordPage()
    {
        return view('pages.restore-password-enter-email');
    }

    public function restorePassword()
    {
        //Input
        $data = Input::except(array('_token'));

        //Validation rules
        $rule = array(
            'email' => 'required'
        );

        //Validation messages
        $messages = array(
            'required' => 'Netika aizpildīti visi lauki!',
        );

        //Validates
        $validator = Validator::make($data, $rule, $messages);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator);
        }

        //Finds the user by the submitted email
        $user = User::where('email', '=', $data['email'])->first();

        if ($user) {
            //Generates token
            $token = uniqid();
            $user->token = $token;
            $user->save();

            //Sends verification email
            $to_name = $user->firstname . ' ' . $user->lastname;
            $to_email = $user->email;
            $mail_data = ['token' => $token];
            Mail::send('emails.verifyresetpassword', $mail_data, function ($message) use ($to_name, $to_email) {
                $message->to($to_email, $to_name)
                    ->subject('Paroles atjaunošana');
                $message->from('tavapat@admin.lv', 'TavaPatversme Administrators');
            });

            return view('pages.verify-reset-password');
        }else{
            Session::flash('message-email-not-found', " ");
            return view('pages.verify-reset-password');
        }
    }

    public function getEnterNewPasswordPage()
    {
        $token = Input::get('token');

        $user = User::where('token', '=', $token)->first();

        if (!$user) {
            Session::flash('error-no-user-found', "");
        }

        return view('pages.restore-password-enter-new-password')->with('token', $token);
    }

    public function setNewPassword()
    {
        //Input
        $data = Input::except(array('_token'));

        //Validation rules
        $rule = array(
            'password' => 'required',
            'token' => 'required',
        );

        //Validation messages
        $messages = array(
            'required' => 'Netika aizpildīti visi lauki!',
        );

        //Validates
        $validator = Validator::make($data, $rule, $messages);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator);
        }

        $token = Input::get('token');

        $user = User::where('token', '=', $token)->first();

        if($user){
            $user->password = bcrypt($data['password']);
            $user->token = '';

            $user->save();

            //Creates a log
            $action = $user->firstname." ".$user->lastname." atjaunoja savu paroli";
            $this->createLog($user->firstname." ".$user->lastname." (".$user->email.")", 1003, $action);
        }

        Session::flash('message-password-reset-success', "Jūsu parole veiksmīgi samainīta!");
        return redirect('/login');
    }

    public function delete()
    {
        $user = User::findOrFail(Auth::user()->id);

        //Generates token
        $token = uniqid();
        $user->token = $token;
        $user->save();

        //Sends verification email
        $to_name = $user->firstname . ' ' . $user->lastname;
        $to_email = $user->email;
        $mail_data = ['token' => $token];
        Mail::send('emails.verifydeletion', $mail_data, function ($message) use ($to_name, $to_email) {
            $message->to($to_email, $to_name)
                ->subject('Profila dzēšana');
            $message->from('tavapat@admin.lv', 'TavaPatversme Administrators');
        });

        return redirect()->route('auth.show_verify_deletion');

    }

    public function getVerifyDeletionPage()
    {
        return view('pages.verify-deletion');
    }

    public function verifyDeletion()
    {
        $token = Input::get('token');

        $user = User::where('token', '=', $token)->first();

        if ($user) {

            //Creates a log
            $action = $user->firstname." ".$user->lastname." izdzēsa sevi";
            $this->createLog($user->firstname." ".$user->lastname." (".$user->email.")", 1002, $action);

            $user->delete();

            return redirect('/login')->withErrors(['Jūsu profils tika izdzēsts!']);
        }

    }

    public function getIndex()
    {
        $categories = DB::table('categories')->get();


        $animals = DB::table('animals')
            ->where('adopted', '=', 0)
            ->get();

        return view('pages.index')
            ->with('categories', $categories)
            ->with('animals', $animals);
    }

    public function getCategoryPage($category_name)
    {
        $categories = DB::table('categories')->get();
        $categoryID = DB::table('categories')->where('name', '=', $category_name)->first()->id;
        $animals = DB::table('animals')->where('category_id', '=', $categoryID)->where('adopted', '=', 0)->get();

        return view('pages.categorypage')
            ->with('category_name', $category_name)
            ->with('categories', $categories)
            ->with('animals', $animals);
    }


}
