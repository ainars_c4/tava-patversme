<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Route;

class WorkerLoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest:worker', ['except' => ['logout']]);
    }

    public function showLoginForm()
    {
        return view('pages.worker.login');
    }

    protected $guard = 'worker';

    public function login(Request $request)
    {

        // Validate the form data
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required'
        ]);

        // Attempt to log the user in
        if (Auth::guard('worker')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
            // if successful, then redirect to their intended location

            return redirect('/worker');
        }
        // if unsuccessful, then redirect back to the login with the form data
        return redirect()->back()->withErrors([
            'message' => 'Nepareizi ievadīta informācija!'
        ]);;
    }

    public function logout()
    {
        Auth::guard('worker')->logout();
        return redirect('/');
    }
}
