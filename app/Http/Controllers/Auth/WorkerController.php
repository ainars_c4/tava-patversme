<?php

namespace App\Http\Controllers;

use App\Models\ActionLog;
use App\Models\Application;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Models\Worker;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use App\Traits\ActionLogger;
use App\Models\Animal;


class WorkerController extends Controller
{

    use ActionLogger;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:worker');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function getWorkerPage()
    {
        if (Auth::guard('worker')->check()) {
            $shelter = DB::table('shelters')->where('id', '=', Auth::guard('worker')->user()->shelter_id)->get();
            $categories = DB::table('categories')->get();
            $animals = DB::table('animals')->where('shelter_id', '=', Auth::guard('worker')->user()->shelter_id)->get();
            $worker = Auth::guard('worker')->user();


            return view('pages.worker.dashboard')
                ->with('shelter', $shelter[0])
                ->with('worker', $worker)
                ->with('categories', $categories)
                ->with('animals', $animals);

        } else {
            return redirect('/worker/login');
        }
    }

    private function isOfSameShelter($WorkerID, $AnimalID)
    {

        //Gets worker Shelter ID
        $WorkerShelterID = DB::table('shelter_workers')->select('shelter_id')->where('id', '=', $WorkerID)->get()[0]->shelter_id;

        //Gets animal Shelter ID
        $AnimalShelterID = DB::table('animals')->select('shelter_id')->where('id', '=', $AnimalID)->get()[0]->shelter_id;

        if ($AnimalShelterID == $WorkerShelterID) {
            return true;
        } else {
            return false;
        }

    }

    public function getMyProfilePage()
    {
        if (Auth::guard('worker')->check()) {
            $shelter = DB::table('shelters')->where('id', '=', Auth::guard('worker')->user()->shelter_id)->get();
            $categories = DB::table('categories')->get();
            $animals = DB::table('animals')->where('shelter_id', '=', Auth::guard('worker')->user()->shelter_id)->get();
            $worker = Auth::guard('worker')->user();

            return view('pages.worker.myprofile')
                ->with('shelter', $shelter[0])
                ->with('worker', $worker)
                ->with('categories', $categories)
                ->with('animals', $animals);

        } else {
            return redirect('/worker/login');
        }
    }

    public function editMe()
    {
        if (Auth::guard('worker')->check()) {
            //Input
            $data = Input::except(array('_token'));

            //Validation rules
            $rule = array(
                'firstname' => 'required',
                'lastname' => 'required'
            );

            //Validation messages
            $messages = array(
                'required' => 'Netika aizpildīti visi lauki!',
            );

            //Validates
            $validator = Validator::make($data, $rule, $messages);

            //Creates a log
            $user = Auth::guard('worker')->user();
            $action = $user->firstname . " " . $user->lastname . " atjaunoja savus datus uz " . $data["firstname"] . " " . $data["lastname"];
            $this->createLog($user->firstname . " " . $user->lastname . " (" . $user->email . ")", 2001, $action, $user->shelter_id);

            //Saves new data
            $user->firstname = $data["firstname"];
            $user->lastname = $data["lastname"];
            $user->save();

            Session::flash('message-edit-successful', "Jūsu dati veiksmīgi rediģēti!");
            return redirect()->back();

        } else {
            redirect('/worker');
        }
    }

    public function editMyPassword()
    {
        if (Auth::guard('worker')->check()) {
            //Input
            $data = Input::except(array('_token'));

            //Validation rules
            $rule = array(
                'password' => 'required',
            );

            //Validation messages
            $messages = array(
                'required' => 'Netika aizpildīti visi lauki!',
            );

            //Validates
            $validator = Validator::make($data, $rule, $messages);

            //Creates a log
            $user = Auth::guard('worker')->user();
            $action = $user->firstname . " " . $user->lastname . " atjaunoja savu paroli";
            $this->createLog(2003, $action, $user->id);

            //Saves new data
            $user->password = bcrypt($data["password"]);
            $user->save();

            Session::flash('message-edit-password-successful', "Jūsu parole veiksmīgi samainīta!");
            return redirect()->back();

        } else {
            redirect('/worker');
        }
    }

    public function getWorkerAdminPage()
    {

        if (Auth::guard('worker')->check() && Auth::guard('worker')->user()->admin == 1) {

            function getShelterStatistics($shelter_id)
            {

                function getMonthName($monthNumber)
                {
                    return date("F", mktime(0, 0, 0, $monthNumber, 1));
                }

                $statistics = [];

                for ($i = 0; $i < 12; $i++) {
                    $dateStartRange = Carbon::now()->firstOfYear()->addMonth($i);
                    $dateEndRange = Carbon::now()->firstOfYear()->addMonth($i + 1);

                    $statistics['current_year']['animals_added'][getMonthName($i + 1)] = ActionLog::where('created_at', '>=', $dateStartRange->toDateTimeString())
                        ->where('created_at', '<=', $dateEndRange->toDateTimeString())
                        ->where('code', '=', '3000')
                        ->where('related_shelter_id', '=', $shelter_id)
                        ->get()->count();

                    $statistics['current_year']['animals_adopted'][getMonthName($i + 1)] = ActionLog::where('created_at', '>=', $dateStartRange->toDateTimeString())
                        ->where('created_at', '<=', $dateEndRange->toDateTimeString())
                        ->where('code', '=', '3003')
                        ->where('related_shelter_id', '=', $shelter_id)
                        ->get()->count();

                    $statistics['current_year']['animals_adopted'][getMonthName($i + 1)] = $statistics['current_year']['animals_adopted'][getMonthName($i + 1)] - ActionLog::where('created_at', '>=', $dateStartRange->toDateTimeString())
                        ->where('created_at', '<=', $dateEndRange->toDateTimeString())
                        ->where('code', '=', '3007')
                        ->where('related_shelter_id', '=', $shelter_id)
                        ->get()->count();
                }

                return $statistics;
            }

            $shelter_statistics = getShelterStatistics(Auth::guard('worker')->user()->shelter_id);

            $shelter = DB::table('shelters')->where('id', '=', Auth::guard('worker')->user()->shelter_id)->get();

            $worker = Auth::guard('worker')->user();

            $shelter_workers = DB::table('shelter_workers')
                ->where('shelter_id', '=', $worker->shelter_id)
                ->where('id', '!=', Auth::guard('worker')->user()->id)
                ->get();

            $action_logs = DB::table('action_logs')
                ->where('related_shelter_id', '=', Auth::guard('worker')->user()->shelter_id)
                ->orderBy('action_logs.created_at', 'desc')
                ->limit(100)
                ->get();

            return view('pages.worker.administration')
                ->with('shelter_statistics', json_encode($shelter_statistics))
                ->with('shelter_workers', $shelter_workers)
                ->with('action_logs', $action_logs)
                ->with('shelter', $shelter[0])
                ->with('worker', $worker);

        } else {
            return redirect('/worker');
        }

    }

    public function addWorker()
    {
        if (Auth::guard('worker')->check() && Auth::guard('worker')->user()->admin == 1) {

            //Input
            $data = Input::except(array('_token'));

            //Cheks if email exists
            $existingWorker = Worker::where('email', '=', $data["email"])->first();
            if ($existingWorker !== null) {
                Session::flash('message-email-already-exists', "Epasts jau eksistē!");
                return redirect()->back();
            }

            //Validation rules
            $rule = array(
                'firstname' => 'required',
                'lastname' => 'required',
                'password' => 'required',
                'email' => 'required|email'
            );

            //Validation messages
            $messages = array(
                'required' => 'Netika aizpildīti visi lauki!',
            );

            //Validates
            $validator = Validator::make($data, $rule, $messages);


            //Creates a new worker
            $worker = new Worker;
            $worker->firstname = $data['firstname'];
            $worker->lastname = $data['lastname'];
            $worker->email = $data['email'];
            $worker->password = bcrypt($data['password']);
            $worker->admin = false;
            $worker->shelter_id = $data['shelter_id'];

            //Saves worker
            $worker->save();

            //Creates a log
            $user = Auth::guard('worker')->user();
            $action = $user->firstname . " " . $user->lastname . " izveidoja jaunu darbinieku (" . $worker->firstname . " " . $worker->lastname . ", e-pasts: " . $worker->email . ")";
            $this->createLog($worker->firstname . " " . $worker->lastname . " (" . $worker->email . ")", 2000, $action, $worker->shelter_id);

            //Paziņojums par veikto darbību
            Session::flash('message-worker-added', "Darbinieks veiksmīgi pievienots!");
            return redirect()->back();

        } else {
            return redirect('/worker');
        }
    }

    public function deleteWorker($id)
    {
        if (Auth::guard('worker')->check() && Auth::guard('worker')->user()->admin == 1) {

            $workerShelterId = DB::table('shelter_workers')->select('shelter_id')->where('id', '=', $id)->get()[0]->shelter_id;

            if (Auth::guard('worker')->user()->shelter_id == $workerShelterId) {
                //Finds the worker
                $worker = Worker::find($id);


                //Creates a log
                $user = Auth::guard('worker')->user();
                $action = $user->firstname . " " . $user->lastname . " izdzēsa darbinieku " . $worker->firstname . " " . $worker->lastname;
                $this->createLog($worker->firstname . " " . $worker->lastname . " (" . $worker->email . ")", 2002, $action, $worker->shelter_id);

                $worker->delete();

                Session::flash('message-worker-deleted', "Darbinieks veiksmīgi dzēsts!");
                return redirect('/worker/administration');
            } else {
                return view('errors.500');
            }

        } else {
            return redirect('/worker');
        }
    }

    public function getEditWorkerPage($id)
    {
        if (Auth::guard('worker')->check() && Auth::guard('worker')->user()->admin == 1) {

            $shelter = DB::table('shelters')->where('id', '=', Auth::guard('worker')->user()->shelter_id)->get();
            $worker = Auth::guard('worker')->user();
            $shelter_worker = DB::table('shelter_workers')->where('id', '=', $id)->first();

            return view('pages.worker.editworker')
                ->with('shelter_worker', $shelter_worker)
                ->with('shelter', $shelter[0])
                ->with('worker', $worker);

        } else {
            return redirect('/worker');
        }
    }

    public function editWorker($id)
    {
        if (Auth::guard('worker')->check() && Auth::guard('worker')->user()->admin == 1) {

            //Input
            $data = Input::except(array('_token'));

            //Cheks if email exists
            $existingWorker = Worker::where('email', '=', $data["email"])->first();
            if ($existingWorker !== null) {
                if ($existingWorker->id !== (int)$id) {
                    Session::flash('message-email-already-exists', "Epasts jau eksistē!");
                    return redirect()->back();
                }
            }

            //Validation rules
            $rule = array(
                'firstname' => 'required',
                'lastname' => 'required',
                'email' => 'required|email'
            );

            //Validation messages
            $messages = array(
                'required' => 'Netika aizpildīti visi lauki!',
            );

            //Validates
            $validator = Validator::make($data, $rule, $messages);

            //Creates a log
            $worker = Worker::find($id);
            $user = Auth::guard('worker')->user();
            $action = $user->firstname . " " . $user->lastname . " atjaunoja darbinieka " . $worker->firstname . " " . $worker->lastname . " datus";
            $this->createLog($worker->firstname . " " . $worker->lastname . " (" . $worker->email . ")", 2101, $action, $worker->shelter_id);

            //Updates worker information
            DB::table('shelter_workers')->where('id', '=', $id)->update([
                'firstname' => $data["firstname"],
                'lastname' => $data["lastname"],
                'email' => $data["email"]
            ]);

            Session::flash('message-worker-edited', "Darbinieks veiksmīgi rediģets!");
            return redirect('/worker/administration');

        } else {
            return redirect('/worker');
        }
    }

    public function editWorkerPassword($id)
    {
        if (Auth::guard('worker')->check() && Auth::guard('worker')->user()->admin == 1) {

            //Input
            $data = Input::except(array('_token'));

            //Validation rules
            $rule = array(
                'password' => 'required',
            );

            //Validation messages
            $messages = array(
                'required' => 'Netika aizpildīti visi lauki!',
            );

            //Validates
            $validator = Validator::make($data, $rule, $messages);

            //Updates worker information
            DB::table('shelter_workers')->where('id', '=', $id)->update([
                'password' => bcrypt($data["password"]),
            ]);

            //Creates a log
            $worker = Worker::find($id);
            $user = Auth::guard('worker')->user();
            $action = $user->firstname . " " . $user->lastname . " atjaunoja darbinieka " . $worker->firstname . " " . $worker->lastname . " paroli";
            $this->createLog($user->firstname . " " . $user->lastname . " (" . $user->email . ")", 2102, $action, $user->shelter_id);

            Session::flash('message-password-changed', "Darbinieka parole veiksmīgi atjaunota!");
            return redirect('/worker/administration');
        } else {
            return redirect('/worker');
        }
    }

    public function getApplicationsPage()
    {
        if (Auth::guard('worker')->check()) {

            $shelter = DB::table('shelters')->where('ID', '=', Auth::guard('worker')->user()->shelter_id)->get();
            $worker = Auth::guard('worker')->user();
            $applications = DB::table('applications')
                ->select('applications.id AS application_id', 'applications.*', 'animals.Name', 'users.*')
                ->join('users', 'users.id', '=', 'applications.UserID')
                ->join('animals', 'animals.ID', '=', 'applications.AnimalID')
                ->where('animals.Shelter_ID', '=', Auth::guard('worker')->user()->shelter_id)
                ->where('applications.Status', '=', 1)
                ->get();

            $resolved_applications = DB::table('applications')
                ->select('applications.id AS application_id', 'applications.*', 'animals.Name', 'users.*')
                ->join('users', 'users.id', '=', 'applications.UserID')
                ->join('animals', 'animals.ID', '=', 'applications.AnimalID')
                ->where('animals.Shelter_ID', '=', Auth::guard('worker')->user()->shelter_id)
                ->where('applications.Status', '!=', 1)
                ->get();

            return view('pages.worker.applications')
                ->with('applications', $applications)
                ->with('resolved_applications', $resolved_applications)
                ->with('shelter', $shelter[0])
                ->with('worker', $worker);

        } else {
            return redirect('/worker');
        }
    }

    public function acceptApplication($ApplicationID)
    {

        $application = Application::find($ApplicationID);

        if (Auth::guard('worker')->check() && $this->isOfSameShelter(Auth::guard('worker')->id(), $application->animalid)) {

            //Rejects the other applications for this animal
            DB::table('applications')->where('animalid', '=', $application->animalid)->update(['status' => 2]);

            //Accepts this application for this animal
            $application->status = 3;
            $application->save();

            //Sets the animal as "Adopted"
            DB::table('animals')->where('id', '=', $application->animalid)->update(['adopted' => 1]);

            //Creates logs
            $animal = Animal::find($application->animalid);
            $client = User::find($application->userid);
            $user = Auth::guard('worker')->user();
            $action = $user->firstname . " " . $user->lastname . " apstiprināja " . $client->firstname . " " . $client->lastname . " adoptācijas pieteikumu dzīvniekam " . $animal->name;
            $this->createLog($user->firstname . " " . $user->lastname . " (" . $user->email . ")", 2004, $action, $user->shelter_id);
            $action = $user->firstname . " " . $user->lastname . " pielika adoptēta dzīvnieka statusu dzīvniekam  " . $animal->name;
            $this->createLog($user->firstname . " " . $user->lastname . " (" . $user->email . ")", 3003, $action, $user->shelter_id);

            Session::flash('message-application-accepted', "Pieteikums veiksmīgi pieņemts!");
            return redirect('/worker/applications');
        } else {
            return redirect('/worker');
        }
    }

    public function rejectApplication($ApplicationID)
    {
        $application = Application::find($ApplicationID);

        if (Auth::guard('worker')->check() && $this->isOfSameShelter(Auth::guard('worker')->id(), $application->animalid)) {

            //Rejects this application for this animal
            DB::table('applications')->where('animalid', '=', $application->animalid)->where('userid', '=', $application->userid)->update(['status' => 2]);

            //Creates a log
            $animal = Animal::find($application->animalid);
            $client = User::find($application->userid);
            $user = Auth::guard('worker')->user();
            $action = $user->firstname . " " . $user->lastname . " noliedza " . $client->firstname . " " . $client->lastname . " adoptācijas pieteikumu dzīvniekam " . $animal->name;
            $this->createLog($user->firstname . " " . $user->lastname . " (" . $user->email . ")", 2005, $action, $user->shelter_id);

            Session::flash('message-application-rejected', "Pieteikums veiksmīgi noliegts!");
            return redirect('/worker/applications');
        } else {
            return redirect('/worker');
        }
    }
}
