<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Route;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * show dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('welcome');
    }

    public function getDashboard()
    {
        $shelters = DB::table('shelters')->get();
        $categories = DB::table('categories')->get();
        return view('pages.admin.dashboard')->with('shelters', $shelters)->with('categories', $categories);
    }

    public function getAdminPage()
    {
        if (Auth::guard('admin')->check()) {
            $shelters = DB::table('shelters')->get();
            $categories = DB::table('categories')->get();
            return view('pages.admin.dashboard')->with('shelters', $shelters)->with('categories', $categories);
        } else {
            return redirect('/admin/login');
        }

    }
}