<?php

namespace App\Http\Controllers\Auth;

use App\Traits\ActionLogger;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use DateTime;

class RegisterController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;
    use ActionLogger;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'Firstname' => 'required',
            'Lastname' => 'required',
            'Phone' => 'required',
            'Street' => 'required',
            'City' => 'required',
            'Street_nr' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'Firstname' => $data['Firstname'],
            'Phone' => $data['Phone'],
            'Lastname' => $data['Lastname'],
            'City' => $data['City'],
            'Street' => $data['Street'],
            'Street_nr' => $data['Street_nr'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

    public function getRegisterPage()
    {
        return view('pages.registration');
    }

    public function registerUser()
    {

        //Input
        $data = Input::except(array('_token'));

        //Validation messages
        $messages = array(
            'required' => 'Netika aizpildīti visi lauki!',
            'email' => 'Nepareizs e-pasts!'
        );

        //Validation rules
        $rule = array(
            'username' => 'required',
            'email' => 'required | email',
            'password' => 'required',
            'firstname' => 'required',
            'lastname' => 'required',
            'phone' => 'required',
            'city' => 'required',
            'street' => 'required'
        );

        //Validates
        $validator = Validator::make($data, $rule, $messages);
        $existing_user = User::where('email', '=', $data['email'])->first();

        if ($existing_user) {
            if (!$existing_user->email_verified_at) {
                $existing_user->delete();
            } else {
                Session::flash('message-email-exists', 'Jūsu ievadītais e-pasts jau tiek izmantots!');
                return redirect('/register');
            }
        }

        //Generates token
        $token = uniqid();

        //Saves user
        $user = User::create([
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'phone' => $data['phone'],
            'city' => $data['city'],
            'street' => $data['street'],
            'street_nr' => $data['street_nr'],
            'token' => $token
        ]);

        //Creates a log
        $action = $user->firstname." ".$user->lastname." tika izveidots";
        $this->createLog($user->firstname." ".$user->lastname." (".$user->email.")", 1000, $action);

        //Sends verification email
        $to_name = $data['firstname'] . ' ' . $data['lastname'];
        $to_email = $data['email'];
        $mail_data = ['token' => $token];
        Mail::send('emails.verifyemail', $mail_data, function ($message) use ($to_name, $to_email) {
            $message->to($to_email, $to_name)
                ->subject('Apstirpiniet savu e-pastu');
            $message->from('tavapat@admin.lv', 'TavaPatversme Administrators');
        });

        //Returns to home
        return redirect()->route('auth.show_verify_email');
    }

    public function getVerifyEmailPage()
    {
        return view('pages.verify-email');
    }

    public function verifyEmail()
    {
        $token = Input::get('token');

        $user = User::where('token', '=', $token)->first();

        if($user){
            $user->email_verified_at = (new DateTime())->format('Y-m-d H:i:s');

            $user->token = null;

            $user->save();

            //Logins user
            auth()->login($user);

            Session::flash('message-email-verified', 'Jūsu e-pasts tika veiksmīgi apstriprināts!');

            return redirect('/');
        }

    }
}
