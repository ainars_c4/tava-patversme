<?php

/**
 * Laravel-admin - admin builder based on Laravel.
 * @author z-song <https://github.com/z-song>
 *
 * Bootstraper for Admin.
 *
 * Here you can remove builtin form field:
 * Encore\Admin\Form::forget(['map', 'editor']);
 *
 * Or extend custom form field:
 * Encore\Admin\Form::extend('php', PHPEditor::class);
 *
 * Or require js and css assets:
 * Admin::css('/packages/prettydocs/css/styles.css');
 * Admin::js('/packages/prettydocs/js/main.js');
 *
 */

use Encore\Admin\Grid\Column;
use App\Models\Shelter;
use App\Models\Category;
use App\Models\Animal;
use App\Models\Disease;

Encore\Admin\Form::forget(['map', 'editor']);

Column::extend('getShelterName', function ($value) {
    $name = Shelter::find($value)['name'];
    return $name;
});

Column::extend('getCategoryName', function ($value) {
    $name = Category::find($value)['name'];
    return $name;
});

Column::extend('getAnimalName', function ($value){
   $name = Animal::find($value)['name'];
   return $name;
});

Column::extend('getDiseaseName', function ($value){
    $name = Disease::find($value)['name'];
    return $name;
});