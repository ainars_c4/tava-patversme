<?php

namespace App\Admin\Controllers;

use App\Models\Animal;
use App\Models\Application;
use App\Http\Controllers\Controller;
use App\User;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Illuminate\Support\Facades\Storage;

class ApplicationController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Pieteikumi')
            ->description(' ')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detaļas')
            ->description(' ')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Mainīt')
            ->description(' ')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Izveidot')
            ->description(' ')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Application);

        $grid->id('ID');
        $grid->userid('Lietotāja ID');
        $grid->animalid('Dzīvnieka ID');
        $grid->answer1('Atbilde 1');
        $grid->answer2('Atbilde 2');
        $grid->picture('Bilde');
        $grid->status('Statuss');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Application::findOrFail($id));

        $show->id('ID');
        $show->userid('Lietotāja ID');
        $show->animalid('Dzīvnieka ID');
        $show->answer1('Atbilde 1');
        $show->answer2('Atbilde 2');
        $show->picture('Bilde');
        $show->status('Statuss');


        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Application);

        $form->select('userid', 'Lietotājs')->options(function () {
            $users = User::all();
            $userList = [];
            foreach ($users as $user) {
                $userList[$user->id] = $user->email;
            }
            return $userList;
        })->required();
        $form->select('animalid', 'Dzīvnieks')->options(function () {
            $animals = Animal::all();
            $animalList = [];
            foreach ($animals as $animal) {
                $animalList[$animal->id] = $animal->name;
            }
            return $animalList;
        })->required();
        $form->textarea('answer1', 'Atbilde 1')->required();
        $form->textarea('answer2', 'Atbilde 2')->required();
        $form->image('picture', 'Bilde');
        $form->select('status', 'Statuss')->options([1 => 'Apstrādā', 2 => "Noliegts", 3 => "Apstiprināts"])->default('1')->required();

        $form->saving(function (Form $form) {
            Storage::delete('public/stored_habitats/' . $form->model()->id . '.jpg');
            $form->image('picture', 'Picture')->move('/stored_habitats')->name($form->model()->id . '.jpg');
        });

        return $form;
    }
}
