<?php

namespace App\Admin\Controllers;

use App\Models\ActionLog;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class ActionLogController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Darbību žurnāls')
            ->description(' ')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detaļas')
            ->description(' ')
            ->body($this->detail($id));
    }


    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new ActionLog);

        $grid->model()->orderBy('created_at', 'desc');

        $grid->performer('Veicējs');
        $grid->code('Kods');
        $grid->action('Darbība');
        $grid->created_at('Izveidots');

        $grid->disableActions();
        $grid->disableRowSelector();

        $grid->filter(function($filter){

            // Remove the default id filter
            $filter->disableIdFilter();

            // Add a column filter

            $filter->where(function ($query) {

                $query->where('related_shelter_id', $this->input);

            }, 'Filtrēt pēc patversmes')->select('/api/get-shelter-names');

            $filter->between('created_at', 'Izveidots')->datetime();

        });

        return $grid;
    }

}
