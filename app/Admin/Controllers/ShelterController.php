<?php

namespace App\Admin\Controllers;

use App\Models\Shelter;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Illuminate\Support\Facades\Storage;

class ShelterController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Patversmju saraksts')
            ->description(' ')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Deataļas')
            ->description(' ')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Mainīt')
            ->description(' ')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Izveidot')
            ->description(' ')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Shelter);

//        $grid->id('id');
        $grid->name('Nosaukums');
        $grid->city('Pilsēta');
        $grid->street('Iela');
        $grid->street_nr('Ielas numurs');
        $grid->picture('Bilde')->image();

        $grid->filter(function($filter){

            // Remove the default id filter
            $filter->disableIdFilter();

            // Add a column filter
            $filter->like('name', 'Nosaukums');
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Shelter::findOrFail($id));

//        $show->id('id');
        $show->name('Nosaukums');
        $show->city('Pilsēta');
        $show->street('Iela');
        $show->street_nr('Ielas numurs');
        $show->picture('Bilde');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Shelter);

//        $form->number('id', 'id');
        $form->text('name', 'Nosaukums');
        $form->text('city', 'Pilsēta');
        $form->text('street', 'Iela');
        $form->text('street_nr', 'Ielas numurs');
        $form->image('picture', 'Bilde');
        $form->hasMany('contactinfo', 'Kontaktinformācija', function (Form\NestedForm $form) {
            $form->text('email', 'E-pasts');
            $form->text('phone', 'Tālruņa numurs');
        });


        $form->saving(function (Form $form) {
            $form->image('picture', 'Picture')->name($form->model()->id.'.jpg')->move('/stored_shelters');
        });

        return $form;
    }
}
