<?php

namespace App\Admin\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Lietotāju saraksts')
            ->description(' ')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Apskatīt')
            ->description(' ')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Mainīt')
            ->description(' ')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Izveidot')
            ->description(' ')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new User);

        $grid->id('ID');
        $grid->firstname('Vārds');
        $grid->lastname('Uzvārds');
        $grid->phone('Tālruņa nr.');
        $grid->city('Pilsēta');
        $grid->email('E-pasts');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(User::findOrFail($id));

        $show->id('ID');
        $show->firstname('Vārds');
        $show->lastname('Uzvārds');
        $show->phone('Tālruņa nr.');
        $show->city('Pilsēta');
        $show->street('Iela');
        $show->street_nr('Ielas numurs');
        $show->email('E-pasts');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new User);

        $form->text('firstname', 'Vārds');
        $form->text('lastname', 'Uzvārds');
        $form->text('phone', 'Tālruņa numurs');
        $form->text('city', 'Pilsēta');
        $form->text('street', 'Iela');
        $form->text('street_nr', 'Ielas numurs');
        $form->email('email', 'E-pasts');
        $form->password('password', 'Parole');
        $form->datetime('email_verified_at', 'E-pasts apstiprināts')->default(date('Y-m-d H:i:s'));

        $form->saving(function (Form $form) {

            if(strlen($form->password) == 0){
                $form->password = $form->model()->password;
            }else{
                $form->password = Hash::make($form->password);
            }
        });

        return $form;
    }
}
