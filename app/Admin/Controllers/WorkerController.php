<?php

namespace App\Admin\Controllers;

use App\Models\Shelter;
use App\Models\Worker;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Illuminate\Support\Facades\Hash;

class WorkerController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Darbinieku saraksts')
            ->description(' ')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Skatīt')
            ->description(' ')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Mainīt')
            ->description(' ')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Izveidot')
            ->description(' ')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Worker);

//        $grid->id('Id');
        $grid->firstname('Vards');
        $grid->lastname('Uzvārds');
        $grid->shelter_id('Patversme')->getShelterName();
        $grid->email('E-pasts');
        $grid->admin('Ir patversmes administrators');

        $grid->filter(function($filter){

            // Remove the default id filter
            $filter->disableIdFilter();

            // Add a column filter
            $filter->like('firstname', 'Vards');
            $filter->like('lastname', 'Uzvārds');
            $filter->equal('shelter_id', 'Patversme')->select('/api/get-shelter-names');
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Worker::findOrFail($id));

        $show->id('ID');
        $show->firstname('Vārds');
        $show->lastname('Uzvārds');
        $show->shelter_id('Patversmes ID');
        $show->email('E-pasts');
        $show->admin('Ir administrators');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Worker);

        $form->text('firstname', 'Vārds');
        $form->text('lastname', 'Uzvārds');
        $form->select('shelter_id', 'Patversme')->options(function(){
            $shelters = Shelter::all();
            $shelterList = [];
            foreach ($shelters as $shelter){
                $shelterList[$shelter->id ] = $shelter->name;
            }
            return $shelterList;
        });
        $form->password('password', 'Parole');
        $form->email('email', 'E-pasts');
        $form->switch('admin', 'Ir patversmes administrators');

        $form->saving(function (Form $form) {

            if(strlen($form->password) == 0){
                $form->password = $form->model()->password;
            }else{
                $form->password = Hash::make($form->password);
            }
        });


        return $form;
    }
}
