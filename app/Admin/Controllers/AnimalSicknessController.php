<?php

namespace App\Admin\Controllers;

use App\Models\Animal;
use App\Models\AnimalHealthHistory;
use App\Http\Controllers\Controller;
use App\Models\Disease;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class AnimalSicknessController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Dzīvnieku veselības ieraksti')
            ->description(' ')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detaļas')
            ->description(' ')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Mainīt')
            ->description(' ')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Izveidot')
            ->description(' ')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new AnimalHealthHistory);

        $grid->animalid('Dzīvnieks')->getAnimalName();
        $grid->sicknessid('Slimība')->getDiseaseName();
        $grid->desc('Apraksts');
        $grid->begindate('Sākuma datums');

        $grid->filter(function($filter){

            // Remove the default id filter
            $filter->disableIdFilter();

            //Add cutom filters
            $filter->equal('animalid', 'Dzīvnieks')->select('/api/get-animal-names');
            $filter->equal('sicknessid', 'Slimība')->select('/api/get-disease-names');

        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(AnimalHealthHistory::findOrFail($id));

        $show->animalid('Animalid');
        $show->sicknessid('Sicknessid');
        $show->desc('Desc');
        $show->begindate('Begindate');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new AnimalHealthHistory);

        $form->select('animalid', 'Animal')->options(function () {
            $animals = Animal::all();
            $animalList = [];
            foreach ($animals as $animal) {
                $animalList[$animal->id] = $animal->name;
            }
            return $animalList;
        });
        $form->select('sicknessid', 'Disease')->options(function () {
            $diseases = Disease::all();
            $diseaseList = [];
            foreach ($diseases as $disease) {
                $diseaseList[$disease->id] = $disease->name;
            }
            return $diseaseList;
        });
        $form->textarea('desc', 'Desc');
        $form->date('begindate', 'Begindate')->default(date('Y-m-d'));

        return $form;
    }
}
