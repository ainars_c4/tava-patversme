<?php

namespace App\Admin\Controllers;

use App\Models\Animal;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Disease;
use App\Models\Shelter;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Illuminate\Support\Facades\Storage;

class AnimalController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Dzīvnieki')
            ->description('Visu pieejamo dzīvnieku saraksts')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detaļas')
            ->description(' ')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Mainīt dzīvnieku')
            ->description(' ')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Izveidošama')
            ->description(' ')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Animal);

        $grid->name('Vārds');
        $grid->category_id('Kategorijas ID');
        $grid->desc('Apraksts');
        $grid->picture('Bilde');
        $grid->shelter_id('Patversmes ID');
        $grid->birth('Dzimšanas datums');
        $grid->adopted('Adoptēts (1 - jā, 0 - nē)');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Animal::findOrFail($id));

        $show->name('Name');
        $show->category_id('Category id');
        $show->desc('Desc');
        $show->picture('Picture');
        $show->shelter_id('Shelter id');
        $show->birth('Birth');
        $show->adopted('Adopted');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {

        $form = new Form(new Animal);

        $form->text('name', 'Vārds');
        $form->select('category_id', 'Kategorija')->options(function () {
            $categories = Category::all();
            $categoryList = [];
            foreach ($categories as $category) {
                $categoryList[$category->id] = $category->name;
            }
            return $categoryList;
        });
        $form->textarea('desc', 'Apraksts');
        $form->image('picture', 'Bilde');
        $form->select('shelter_id', 'Patversme')->options(function () {
            $shelters = Shelter::all();
            $shelterList = [];
            foreach ($shelters as $shelter) {
                $shelterList[$shelter->id] = $shelter->name;
            }
            return $shelterList;
        });
        $form->date('birth', 'Dzimšanas datums')->default(date('Y-m-d'));
        $form->switch('adopted', 'Adoptēts');

        $form->saving(function (Form $form) {
            Storage::delete('public/stored_animals/' . $form->model()->id . '.jpg');
            $form->image('picture', 'Picture')->move('/stored_animals')->name($form->model()->id . '.jpg');
        });


        return $form;
    }
}
