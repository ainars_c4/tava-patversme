<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Models\ActionLog;
use App\Models\Animal;
use App\User;
use Carbon\Carbon;
use App\Admin\Controllers\Dashboard\Dashboard;
use Encore\Admin\Layout\Column;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;
use Encore\Admin\Widgets\Box;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    function getMonthName($monthNumber)
    {
        return date("F", mktime(0, 0, 0, $monthNumber, 1));
    }

    private function getChartData($chart_label)
    {
        $data = [];
        switch ($chart_label) {
            case 'Dzīvnieku statistika':

                //Data for the last month
                $data['last_month']['adopted_animal_count'] = ActionLog::where('created_at', '>=', Carbon::now()->firstOfMonth()->toDateTimeString())
                    ->where('code', '=', 3003)
                    ->get()->count();

                $data['last_month']['adopted_animal_count'] = $data['last_month']['adopted_animal_count'] - ActionLog::where('created_at', '>=', Carbon::now()->firstOfMonth()->toDateTimeString())
                        ->where('code', '=', 3007)
                        ->get()->count();

                $data['last_month']['added_animal_count'] = ActionLog::where('created_at', '>=', Carbon::now()->firstOfMonth()->toDateTimeString())
                    ->where('code', '=', 3000)
                    ->get()->count();


                //Data overall
                $data['overall']['animals_added_count'] =
                    ActionLog::where('code', '=', 3000)
                        ->get()->count();

                $data['overall']['adopted_animal_count'] = ActionLog::where('code', '=', 3003)
                    ->get()->count();

                $data['overall']['adopted_animal_count'] = $data['overall']['adopted_animal_count'] - ActionLog::where('code', '=', 3007)
                        ->get()->count();

                $data['overall']['animals_available_for_adoption'] = Animal::where('adopted', '!=', 1)
                    ->get()->count();

                $data['overall']['animals_available_for_adoption_with_diseases'] = DB::table('animal_health_history')
                    ->leftJoin('animals', 'animal_health_history.animalid', '=', 'animals.id')
                    ->where('animals.adopted', '!=', 1)
                    ->get()->count();

                return $data;
                break;
            case 'Lietotāju statistika':

                $data = [];

                for ($i = 0; $i < 12; $i++) {
                    $dateStartRange = Carbon::now()->firstOfYear()->addMonth($i);
                    $dateEndRange = Carbon::now()->firstOfYear()->addMonth($i + 1);

                    $data['last_year']['users_created'][$this->getMonthName($i + 1)] = ActionLog::where('created_at', '>=', $dateStartRange->toDateTimeString())
                        ->where('created_at', '<=', $dateEndRange->toDateTimeString())
                        ->where('code', '=', '1000')
                        ->get()->count();

                    $data['last_year']['users_deleted'][$this->getMonthName($i + 1)] = ActionLog::where('created_at', '>=', $dateStartRange->toDateTimeString())
                        ->where('created_at', '<=', $dateEndRange->toDateTimeString())
                        ->where('code', '=', '1002')
                        ->get()->count();

                }

                $data['overall']['amount_of_users'] = User::all()->count();

                return $data;

                break;
            default:
                return null;
                break;
        }
    }

    public function index(Content $content)
    {
        return $content
            ->header('Darba virsma')
            ->row(function (Row $row) {
                $row->column(6, function (Column $column) {
                    $column->append(new Box('Dzīvnieku statistika', view('admin.adopted_chart')->with('data', $this->getChartData('Dzīvnieku statistika'))));
                });
                $row->column(6, function (Column $column) {
                    $column->append(new Box('Lietotāju statistika', view('admin.users_chart')->with('data', $this->getChartData('Lietotāju statistika'))));
                });
            })
            ->row(function (Row $row) {

                $row->column(4, function (Column $column) {
                    $column->append(Dashboard::environment());
                });
            });
    }
}
