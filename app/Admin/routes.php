<?php

use Illuminate\Routing\Router;

Admin::registerAuthRoutes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/', 'HomeController@index');

    $router->resource('/users', UserController::class);

    $router->resource('/shelters', ShelterController::class);

    $router->resource('/workers', WorkerController::class);

    $router->resource('/logs', ActionLogController::class);

    $router->resource('/animals', AnimalController::class);

    $router->resource('/disease', DiseaseController::class);

    $router->resource('/animal-health-history', AnimalSicknessController::class);

    $router->resource('/applications', ApplicationController::class);

    $router->resource('/categories', CategoryController::class);
});
