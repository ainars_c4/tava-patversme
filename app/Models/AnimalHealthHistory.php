<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AnimalHealthHistory extends Model
{
    public $timestamps = false;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'animal_health_history';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'animalid', 'sicknessid', 'desc', 'begindate',
    ];
}
