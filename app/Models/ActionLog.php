<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActionLog extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'action_logs';

    public $timestamps = false;

    protected $fillable = [
        'code', 'descripton', 'performer', 'created_at', 'related_shelter_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
}
