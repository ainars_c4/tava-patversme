<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contactinfo extends Model
{
    public $timestamps = false;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'contact_info';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'phone', 'shelter_id',
    ];

    public function shelter()
    {
        return $this->belongsTo(Shelter::class, 'id');
    }
}
