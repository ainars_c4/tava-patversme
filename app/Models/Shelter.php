<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Shelter extends Model
{

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'city', 'street', 'street_nr', 'picture',
    ];

    public function contactinfo()
    {
        return $this->hasMany(Contactinfo::class, 'shelter_id');
    }

    public function workers()
    {
        return $this->hasMany(Worker::class, 'shelter_id');
    }
}
