<?php namespace App\Traits;

use App\Models\ActionLog;

trait ActionLogger
{
    public function createLog($performer, $code, $action, $related_shelter_id = null, $created_at = null)
    {
        $new_log = new ActionLog();

        $new_log->performer = $performer;
        $new_log->code = $code;
        $new_log->action = $action;
        $new_log->related_shelter_id = $related_shelter_id;
        $new_log->created_at = ($created_at) ? $created_at : new \DateTime("now", new \DateTimeZone('Europe/Riga'));

        $new_log->save();

    }
}