function initFileInputs() {

    /*
	By Osvaldas Valutis, www.osvaldas.info
	Available for use under the MIT License
    */

    $('.inputfile').each(function () {
        var $input = $(this),
            $label = $input.next('label'),
            labelVal = $label.html();
        $input.on('change', function (e) {
            var fileName = '';

            if (this.files && this.files.length > 1)
                fileName = ( this.getAttribute('data-multiple-caption') || '' ).replace('{count}', this.files.length);
            else if (e.target.value)
                fileName = e.target.value.split('\\').pop();

            if (fileName)
                $label.find('span').html(fileName);
            else
                $label.html(labelVal);
        });

        // Firefox bug fix

        $input.on('focus', function () {
            $input.addClass('has-focus');
        });
        $input.on('blur', function () {
            $input.removeClass('has-focus');
        });
    });
}


$(document).ready(function () {
    //Parsley language
    Parsley.addMessages('lv', {
        defaultMessage: "Šis ieraksts veikts nekorekti.",
        type: {
            email: "Šeit jāieraksta derīgs e-pasts.",
            url: "Šeit jāieraksta korekts url.",
            number: "Šeit jāieraksta derīgs skaitlis.",
            integer: "Šeit jāieraksta vesels skaitlis.",
            digits: "Šeit jāieraksta cipari.",
            alphanum: "Šeit derīgi tikai alfabēta burti vai cipari."
        },
        notblank: "Šis ieraksts nedrīkst būt tukšs.",
        required: "Šis ieraksts ir obligāti jāaizpilda.",
        pattern: "Šis ieraksts aizpildīts nekorekti.",
        min: "Šai vērtībai jābūt lielākai vai vienādai ar %s.",
        max: "Šai vērtībai jābūt mazākai vai vienādai ar %s.",
        range: "Šai vērtībai jābūt starp %s un %s.",
        minlength: "Vērtībai jābūt vismaz %s simbolu garai.",
        maxlength: "Vērtībai jābūt %s simbolus garai vai īsākai.",
        length: "Šīs vērtības garums ir neatbilstošs. Tai jābūt %s līdz %s simbolus garai.",
        mincheck: "Jāizvēlas vismaz %s varianti.",
        maxcheck: "Jāizvēlas %s varianti vai mazāk.",
        check: "Jāizvēlas no %s līdz %s variantiem.",
        equalto: "Šai vērtībai jāsakrīt."
    });

    Parsley.setLocale('lv');

    initFileInputs();

    //Forms
    if ($('.form-box').length) {
        $('.form-box').parsley();
        $('.form-box').find('[type="submit"]').on('click', function (e) {
            $form = $(this).closest('form');
            e.preventDefault();
            $form.parsley().validate();
            if ($form.parsley().isValid()) {
                $form.submit();
            }
        });
    }
    if ($('[data-file-open]').length) {
        $('[data-file-open]').each(function () {
            var $thisObject = $(this);
            var id = $thisObject.attr('data-file-open');

            $thisObject.find('[data-change-file]').click(function () {
                $('[data-file-closed="' + id + '"]').show();
                $('[data-add-required]').prop('required', true);
                $thisObject.hide();
            });
        });
    }
    $('.tp-select2-type-1').select2({
        language: "lv",
        width: "100%"
    });
    $('[data-animal-birth]').datepicker({
        autoHide: true,
        format: 'yyyy-mm-dd',
        language: 'lv-LV',
        weekStart: 1
    });
    $('[data-sickness-date]').datepicker({
        autoHide: true,
        format: 'yyyy-mm-dd',
        language: 'lv-LV',
        weekStart: 1
    });

    $('[data-show]').each(function () {
        var $thisElement = $(this);
        var $targetElement = $('#' + $thisElement.data('show'));

        $thisElement.click(function () {
            $thisElement.hide();
            $targetElement.show();
        });
    });

    $('[data-force-validation]').each(function () {
        $(this).parsley().validate();
    });

    $('.full-image-link').magnificPopup({
        type: 'image',
        tClose: 'Aizvērt',
        tLoading: 'Ielāde...',
    });

    $('[data-shelter-statistics-chart]').each(function () {
        var $thisChart = $(this);

        var chartData = $thisChart.data('shelter-statistics-chart');

        var addedAnimals = [];
        for (var key in chartData.current_year.animals_added) {
            addedAnimals.push(chartData.current_year.animals_added[key]);
        }

        var adoptedAnimals = [];
        for (var key in chartData.current_year.animals_adopted) {
            adoptedAnimals.push(chartData.current_year.animals_adopted[key]);
        }

        var ctx = $thisChart[0].getContext('2d');
        ctx.height = 300;
        var myChart = new Chart(ctx, {
            type: 'line',
            responsive: true,
            options: {
                maintainAspectRatio: false,
            },
            data: {
                labels: [
                    "Janvāris",
                    "Februāris",
                    "Marts",
                    "Aprīlis",
                    "Maijs",
                    "Jūnijs",
                    "Jūlijs",
                    "Augusts",
                    "Septembris",
                    "Oktobris",
                    "Novermbris",
                    "Decembris"

                ],
                datasets: [
                    {
                        label: "Dzīvnieki izveidoti",
                        backgroundColor: [
                            'rgba(0,166,90,0.2)'
                        ],
                        borderColor: [
                            '#00a65a'
                        ],
                        data: addedAnimals
                    },
                    {
                        label: "Dzīvnieki adoptēti",
                        fillColor: "rgba(151,187,205,0.2)",
                        strokeColor: "rgba(151,187,205,1)",
                        pointColor: "rgba(151,187,205,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(151,187,205,1)",
                        data: adoptedAnimals
                    }
                ]
            },
        });
    });
});
