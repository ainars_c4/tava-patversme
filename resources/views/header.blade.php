<header class="header-row">
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="/">
                <img class="header-logo" src="/img/TavaPatLogo.png" alt="">
            </a>
            <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse spacer-sm-top-20 spacer-lg-top-0" id="navbarNav">
                <ul class="navbar-nav ml-auto">
                    @if(strpos($_SERVER['REQUEST_URI'], 'worker') !== false)
                        @if(Auth::guard('worker')->check())
                            <li class="nav-item">
                                <span class="nav-link">{{Auth::guard('worker')->user()->firstname}} {{Auth::guard('worker')->user()->lastname}}</span>
                            </li>
                            <li class="nav-item">
                                <a href="/worker/my-profile" class="nav-link"><i class="user"></i>Mans profils</a>
                            </li>
                            <li class="nav-item">
                                <a href="/worker/logout" class="nav-link">Iziet</a>
                            </li>
                        @endif
                    @else
                        @if(Auth::guard('web')->check())
                            <li class="nav-item">
                                <span class="nav-link">{{Auth::user()->firstname}} {{Auth::user()->lastname}}</span>
                            </li>
                            <li class="nav-item">
                                <a href="/profile" class="nav-link"><i class="user"></i>Mans profils</a>
                            </li>
                            <li class="nav-item">
                                <a href="/logout" class="nav-link">Iziet</a>
                            </li>
                        @else
                            <li class="nav-item">
                                <a href="/login" class="nav-link"><i class="user"></i>Ieiet</a>
                            </li>
                            <li class="nav-item">
                                <a href="/register" class="nav-link">Reģistrēties</a>
                            </li>
                        @endif
                    @endif
                </ul>


            </div>
        </nav>
    </div>
</header>

