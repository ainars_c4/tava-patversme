<div style="margin-bottom: 30px">
    <h4>Tekošais gads:</h4>
    <canvas id="userStatisticsChart" width="400" height="300"></canvas>
</div>

<div>
    <h4>Vispārīgi:</h4>
    <table class="table table-striped">
        <tbody>
        <tr>
            <td width="120px">Esošo lietotāju daudzums</td>
            <td>{{ $data['overall']['amount_of_users'] }}</td>
        </tr>
        </tbody>
    </table>
</div>

<script>
    $(function () {
        var ctx = document.getElementById("userStatisticsChart").getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: [
                    @foreach ($data['last_year']['users_created'] as $key => $value)
                        "{{ $key }}",
                    @endforeach
                ],
                datasets: [
                    {
                        label: "Lietotāji izveidoti",
                        backgroundColor: [
                            'rgba(0,166,90,0.2)',
                        ],
                        borderColor: [
                            '#00a65a',
                        ],
                        data: [
                            @foreach ($data['last_year']['users_created'] as $key => $value)
                            {{ $value }},
                            @endforeach
                        ]
                    },
                    {
                        label: "Lietotāji dzēsti",
                        fillColor: "rgba(151,187,205,0.2)",
                        strokeColor: "rgba(151,187,205,1)",
                        pointColor: "rgba(151,187,205,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(151,187,205,1)",
                        data: [
                            @foreach ($data['last_year']['users_deleted'] as $key => $value)
                            {{ $value }},
                            @endforeach
                        ]
                    }
                ]
            },
        });
    });
</script>