<div style="margin-bottom: 30px">
    <h4>Tekošais mēnesis:</h4>
    <canvas id="animalStatisticsChart" width="400" height="300"></canvas>
</div>

<div>
    <h4>Vispārīgi:</h4>
    <table class="table table-striped">
        <tbody>
        <tr>
            <td width="160px">Adopteti dzīvnieki</td>
            <td>{{ $data['overall']['adopted_animal_count'] }}</td>
        </tr>
        <tr>
            <td width="160px">Jauni dzīvnieki</td>
            <td>{{ $data['overall']['animals_added_count'] }}</td>
        </tr>
        <tr>
            <td width="160px">Dzīvnieki pieejami adoptācijai</td>
            <td>{{ $data['overall']['animals_available_for_adoption'] }}</td>
        </tr>
        <tr>
            <td width="160px">Dzīvnieki pieejami adoptācijai <b>(ar jebkādu slimības vēsturi)</b></td>
            <td>{{ $data['overall']['animals_available_for_adoption_with_diseases'] }}</td>
        </tr>
        </tbody>
    </table>
</div>

<script>
    $(function () {
        var ctx = document.getElementById("animalStatisticsChart").getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'doughnut',
            data: {
                labels: ["Animals adopted", "Animals added"],
                datasets: [{
                    data: [{{ $data['last_month']['adopted_animal_count'] }}, {{ $data['last_month']['added_animal_count'] }}],
                    backgroundColor: [
                        'rgba(127,127,127, 0.2)',
                        'rgba(0,166,90,0.2)',
                    ],
                    borderColor: [
                        'rgb(127,127,127)',
                        '#00a65a',
                    ],
                    borderWidth: 1
                }]
            },
        });
    });
</script>