Lai pabeigtu konta dzēšanu, lūdzu atveriet šo saiti:
<br>
<a href="{{ route('auth.verify_deletion', ['token' => $token]) }}">
    {{ route('auth.verify_deletion', ['token' => $token]) }}
</a>