Lai pabeigtu paroles atjaunošanu, lūdzu atveriet šo saiti:
<br>
<a href="{{ route('auth.verify_reset_password', ['token' => $token]) }}">
    {{ route('auth.verify_reset_password', ['token' => $token]) }}
</a>