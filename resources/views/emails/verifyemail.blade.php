Lai pabeigtu reģistrāciju, lūdzu atveriet šo saiti:
<br>
<a href="{{ route('auth.verify_email', ['token' => $token]) }}">
    {{ route('auth.verify_email', ['token' => $token]) }}
</a>