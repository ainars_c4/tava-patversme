@extends('base')

@section('title', 'Pierakstīšanās')

@section('content')

    <section class="centralblockform">
        <div class="contentbox">
            @if(Session::has('message-email-not-found'))
                <h1>E-pasts netika atrasts!</h1>
                <p>Ja Jūs esat pārliecināts par to, ka esat reģistrēti ar šo e-pastu, bet tā pat redzat šo ķļūdu, lūdzu sazinaties ar mums! E-pasts: <a href="mailto: info@tavapat.lv">info@tavapat.lv</a></p>
            @else
                <h2 class="box-title">Paroles atjaunošana</h2>
                <p>
                    Lai pabeigtu paroles atjaunošanu, lūdzu dodaties uz savu epastu un sekojiet mūsu ziņas instrukcijām.
                </p>
            @endif
        </div>
    </section>
@endsection
