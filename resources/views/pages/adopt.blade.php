@extends('base')

@section('title', 'Adoptcija')

@section('content')
    <section class="base-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <a class="btn-back-type-1 spacer-sm-bottom-20" href="/view-animal/{{ $animal->id }}">Atpakaļ</a>
                </div>
                <div class="col-12">
                    <h2>Dzīvnieka pieteikšana</h2>
                </div>
            </div>
        </div>
    </section>
    <section class="base-section">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="row">
                        <div class="col-12 col-lg-6 spacer-md-bottom-30">
                            <img class="image-type-3" src="/storage/{{$animal->picture}}" alt="img">
                        </div>
                        <div class="col-12 col-lg-6 spacer-sm-bottom-30">
                            <h3>{{$animal->name}}</h3>
                            <p>Vecums: {{ \Carbon\Carbon::parse($animal->birth)->diff(\Carbon\Carbon::now())->format('%y gadi un %m mēneši') }}</p>
                        </div>
                        <div class="col-12">
                            <h4>Veselības vēsture</h4>
                            @if(count($allAnimalDiseases))
                                @foreach($allAnimalDiseases as $disease)
                                    <div class="disease-table-type-1 spacer-sm-bottom-20">
                                        <div class="part-1">
                                            <div class="title">Slimība</div>
                                            <div class="disease">{{ $disease->name }}</div>
                                        </div>
                                        <div class="part-2">
                                            <div class="title">Apraksts</div>
                                            <div class="description">
                                                {{ $disease->desc }}
                                            </div>
                                        </div>
                                        <div class="part-3">
                                            <div class="diagnosis">
                                                Diagnozes datums: <b>{{ $disease->begindate }}</b>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                <p>Dzīvnieks nekad nav slimojis!</p>
                            @endif
                        </div>
                        <div class="col-12">
                            <h4>Apraksts</h4>
                            <p>{{ $animal->desc }}</p>
                        </div>
                    </div>

                </div>
                <div class="col-12 col-md-6">
                    <h3>Pieteikuma anketa</h3>
                    <p>
                        Lai jūs varētu adoptēt dzīvnieku, jums ir jāaizpilda šī anketa, lai pārliecinātos par to, ka jūs esat piemērots dzīvnieka adoptācijai.
                        Šis ir nepieciešams pamatā uz <a href="https://likumi.lv/doc.php?id=132534">Ministru kabineta noteikumu Nr.266</a>.
                    </p>
                    <form @if(count($errors)) data-force-validation @endif class="form-box" enctype="multipart/form-data" method="post" action="/make-application/{{ $animal->id }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <label for="Answer1">Cik bieži jūs kontaktēsieties ar savu dzīvnieku un kāpēc?</label>
                        <textarea required name="answer1" id="answer1" class="tp-textarea-type-1">{{ old('answer1') }}</textarea>

                        <label for="Answer2">Ar ko jūs apgādāsiet savu dzīvnieku un kāpēc?</label>
                        <textarea required name="answer2" id="answer2" class="tp-textarea-type-1">{{ old('answer2') }}</textarea>

                        <label for="Picture">Dzīvnieka uzturēsanas vietas bilde</label>
                        <div class="box spacer-sm-bottom-30">
                            <input required name="picture" type="file" id="picture" class="inputfile inputfile-1" data-parsley-errors-container="#error-1" data-add-required/>
                            <label for="picture"><span>Pievienojiet bildi</span></label>
                            <div id="error-1"></div>
                        </div>


                        <input class="tp-btn-type-1" type="submit" value="Nosūtīt pieteikumu">

                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection