@extends('base')

@section('title', 'Galvenā lapa')

@section('content')
    <section class="base-section">
        <div class="container">
            <div class="container-type-1">
                <div class="sidebar">
                    <div class="sidebar-title">Kategorijas</div>
                    <ul>
                        @foreach($categories as $category)
                            <li><a  @if(strpos(urldecode($_SERVER['REQUEST_URI']), $category->name)) class="active" @endif href="/category/{{$category->name}}">{{$category->name}}</a></li>
                        @endforeach
                    </ul>
                </div>
                <div class="content">
                    <h2>{{ $category_name }}</h2>
                    <div class="animal-table-type-2">
                        @foreach ($animals as $animal)
                            <div class="animal-card">
                                <div class="animal-image">
                                    <img src="/storage/{{ $animal->picture }}" alt="Pic">
                                </div>
                                <div class="animal-name">
                                    {{ $animal->name }}
                                </div>
                                <div class="animal-actions">
                                    <a class="btn btn-primary" href="/view-animal/{{ $animal->id }}">Apskatīt</a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
