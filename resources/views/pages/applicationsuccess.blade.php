@extends('base')

@section('title', 'Paldies!')

@section('content')
    <section class="base-section">
        <div class="container">
            <div class="full-page-cover">
                <div class="row">
                    <div class="col-12">
                        <h2>Paldies par jūsu pieteikumu!</h2>
                        <p>Jūsu pieteikums būs apskatīts nākamās nedēļas laikā! Jūs varat aplūkot jūsu pieteikumus un tā stāvokli <a href="/profile">jūsu profila lapā!</a></p>
                        <div class="d-flex justify-content-center">
                            <a href="/" class="tp-btn-type-1">Uz sākumlapu</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection