@extends('base')

@section('title', 'Pierakstīšanās')

@section('content')

    <section class="centralblockform">
        <div class="contentbox">
            <h2 class="box-title">Reģistrācija veiksmīga!</h2>
            <p>
                Lietotāja reģistrācija veiksmīga!
                <br><br>
                Lūdzu pārbaudiet savu e-pastu, lai ativizētu kontu.
            </p>
        </div>
    </section>
@endsection
