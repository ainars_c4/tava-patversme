@extends('base')

@section('title', 'Reģistrācija')

@section('content')

    <section>
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-3">
                    <ul class="sidebar-type-2">
                        <li><a href="#my_data">Mainīt savus datus</a></li>
                        <li><a href="#change_password">Mainīt paroli</a></li>
                        <li><a href="#delete_profile">Dzēst profilu</a></li>
                        <li><a href="#my_applications">Mani pieteikumi</a></li>
                    </ul>
                </div>
                <div class="col-12 col-md-9">
                    <section id="my_data" class="base-section spacer-sm-bottom-30">
                        <div class="container">
                            <div class="row">
                                <div class="col-12">
                                    <h1 class="box-title">Mani dati</h1>
                                </div>
                                <div class="col-12">
                                    <h3 class="box-title">Mainīt savus datus</h3>
                                </div>
                                <div class="col-12">
                                    @if(Session::has('edit-success'))
                                        <div class="succesful">
                                            {{Session::get('edit-success')}}
                                        </div>
                                    @endif
                                    <form @if(count($errors)) data-force-validation @endif action="/edit-user" method="post" class="form-box">
                                        <div class="row">
                                            <div class="col-sm-12 col-md-6">
                                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                                <div class="form-group">
                                                    <label for="firstname">Vārds</label>
                                                    <input class="tp-input-type-1" required type="text" name="firstname" value="{{Auth::user()->firstname}}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="lastname">Uzvārds</label>
                                                    <input class="tp-input-type-1" required type="text" name="lastname" value="{{Auth::user()->lastname}}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="phone">Tālruņa nr.</label>
                                                    <input class="tp-input-type-1" required type="text" name="phone" value="{{Auth::user()->phone}}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="city">Pilsēta</label>
                                                    <input class="tp-input-type-1" required type="text" name="city" value="{{Auth::user()->city}}">
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6">
                                                <div class="form-group">
                                                    <label for="street">Iela</label>
                                                    <input class="tp-input-type-1" required type="text" name="street" value="{{Auth::user()->street}}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="street_nr">Ielas nr.</label>
                                                    <input class="tp-input-type-1" type="text" name="street_nr" value="{{Auth::user()->street_nr}}">
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="d-flex justify-content-center">
                                                    <input class="tp-btn-type-1" type="submit" value="Saglabāt">
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </section>

                    <section id="change_password" class="base-section spacer-sm-bottom-30">
                        <div class="container">
                            <div class="row">
                                <div class="col-12">
                                    <h3 class="box-title">Mainīt paroli</h3>

                                    @if(Session::has('edit-password-success'))
                                        <div class="succesful">
                                            {{Session::get('edit-password-success')}}
                                        </div>
                                    @endif
                                </div>
                                <div class="col-6">
                                    <form @if(count($errors)) data-force-validation @endif action="/edit-password" method="post" class="form-box">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">

                                        <div class="form-group">
                                            <label for="password">Parole</label>
                                            <input class="tp-input-type-1" required type="password" name="password" id="password">
                                        </div>
                                        <div class="form-group">
                                            <label for="password_repeat">Atkārtoti ievadiet paroli</label>
                                            <input class="tp-input-type-1" type="password" name="password_repeat" required data-parsley-equalto="#password">
                                        </div>

                                        <input type="submit" class="tp-btn-type-1" value="Mainīt">

                                    </form>
                                </div>
                            </div>
                        </div>
                    </section>

                    <section id="delete_profile" class="base-section spacer-sm-bottom-30">
                        <div class="container">
                            <div class="row">
                                <div class="col-12">
                                    <h3 class="box-title">Dzēst profilu</h3>
                                    {{--<p>Right to be Forgotten--}}
                                    {{--Also known as Data Erasure, the right to be forgotten entitles the data subject to have the data controller erase his/her personal data, cease further dissemination of the data, and potentially have third parties halt processing of the data. The conditions for erasure, as outlined in article 17, include the data no longer being relevant to original purposes for processing, or a data subject withdrawing consent. It should also be noted that this right requires controllers to compare the subjects’ rights to “the public interest in the availability of the data” when considering such requests.</p>--}}
                                    <a href="/delete-user" class="tp-btn-type-1">Dzēst sevi</a>
                                </div>
                            </div>
                        </div>
                    </section>

                    <section id="my_applications" class="base-section spacer-sm-bottom-30">
                        <div class="container">
                            <div class="row">
                                <div class="col-12">
                                    <h3>Mani pieteikumi</h3>
                                </div>
                                <div class="col-12">
                                    @foreach($applications as $application)
                                        <div class="row container-type-2">
                                            <div class="col-12 col-lg-2">
                                                <div>
                                                    <b>Dzīvnieks:</b>
                                                </div>
                                                <a href="/view-animal/{{ $application->animalid }}">{{ $application->name }}</a>

                                                <div>
                                                    <b>Statuss: </b>
                                                    @if($application->status == 1)
                                                        Apstrādā
                                                    @endif
                                                    @if($application->status == 2)
                                                        Noliegts
                                                    @endif
                                                    @if($application->status == 3)
                                                        Apstiprināts
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-12 col-lg-5">
                                                <p><b>Cik bieži tiks uzturēts kontakts ar dzīvnieku un kāpēc?</b></p>
                                                <p>{{ $application->answer1 }}</p>

                                                <p><b>Cik bieži tiks uzturēts kontakts ar dzīvnieku un kāpēc?</b></p>
                                                <p>{{ $application->answer2 }}</p>
                                            </div>
                                            <div class="col-12 col-lg-5">
                                                <p><b>Dzvnieka uzturēšanās vietas attēls</b></p>
                                                <a href="/storage/{{ $application->picture }}" class="full-image-link">
                                                    <img class="image-type-3" src="/storage/{{ $application->picture }} " alt="img">
                                                </a>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </section>


@endsection
