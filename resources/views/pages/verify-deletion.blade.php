@extends('base')

@section('title', 'Pierakstīšanās')

@section('content')

    <section class="centralblockform">
        <div class="contentbox">
            <h2 class="box-title">Konta dzēšana</h2>
            <p>
                Lai pabeigtu profila dzēšanu, lūdzu apstiprinat to e-pastā.
            </p>
        </div>
    </section>
@endsection
