@extends('base')

@section('title', 'Darbinieks')

@section('content')

    @include('pages.worker.modules.navbar')

    <section class="base-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2>Dzīvnieku pieteikumi</h2>
                </div>
            </div>
        </div>
    </section>
    <section class="base-section">
        <div class="container">
            <div class="row">
                <div class="col-12 spacer-sm-bottom-30">

                    <h3>Neapstrādātie pieteikumi</h3>

                    @if (Session::has('message-application-accepted'))
                        <div class="col-12 spacer-sm-top-20">
                            <div class="alert alert-info alert-dismissible fade show" role="alert">
                                {{ Session::get('message-application-accepted') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    @endif
                    @if (Session::has('message-application-rejected'))
                        <div class="col-12 spacer-sm-top-20">
                            <div class="alert alert-info alert-dismissible fade show" role="alert">
                                {{ Session::get('message-application-rejected') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    @endif

                    @if(Count($applications))
                        @foreach($applications as $application)
                            <div class="row container-type-2">
                                <div class="col-12 col-lg-2">
                                    <div class="spacer-sm-bottom-20">
                                        <div>
                                            <b>Dzīvnieks:</b>
                                        </div>
                                        <a href="/view-animal/{{ $application->animalid }}">{{ $application->Name }}</a>
                                    </div>

                                    <div class="container-type-4">
                                        <b>Klients:</b>
                                        <p>
                                            {{$application->firstname}} {{$application->lastname}} <br>
                                            <b>E-pasts:</b> <br>
                                            <a href="mailto: {{ $application->email }}">{{ $application->email }}</a> <br>
                                            <b>Tālruņa nr.</b> <br>
                                            <a href="tel: {{ $application->phone }}">{{ $application->phone }}</a> <br>
                                        </p>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-5">
                                    <p><b>Cik bieži tiks uzturēts kontakts ar dzīvnieku un kāpēc?</b></p>
                                    <p>{{ $application->answer1 }}</p>

                                    <p><b>Cik bieži tiks uzturēts kontakts ar dzīvnieku un kāpēc?</b></p>
                                    <p>{{ $application->answer2 }}</p>
                                </div>
                                <div class="col-12 col-lg-5">
                                    <p><b>Dzvnieka uzturēšanās vietas attēls</b></p>
                                    <a href="/storage/{{ $application->picture }}" class="full-image-link">
                                        <img class="image-type-3" src="/storage/{{ $application->picture }} " alt="img">
                                    </a>
                                </div>
                                <div class="col-12">
                                    <a href="/worker/applications/accept/{{ $application->application_id }}" class="btn btn-primary">Pieņemt</a>
                                    <a href="/worker/applications/deny/{{ $application->application_id }}" class="btn btn-primary">Noliegt</a>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <p>Nav neviena pieteikuma!</p>
                    @endif
                </div>

                <div class="col-12">

                    <h3>Apstrādātie pieteikumi pieteikumi</h3>

                    @if(Count($resolved_applications))
                        @foreach($resolved_applications as $application)
                            <div class="row container-type-2">
                                <div class="col-12 col-lg-2">
                                    <div class="spacer-sm-bottom-20">
                                        <div>
                                            <b>Dzīvnieks:</b>
                                        </div>
                                        <a href="/view-animal/{{ $application->animalid }}">{{ $application->Name }}</a>
                                    </div>

                                    <div class="container-type-4">
                                        <b>Klients:</b>
                                        <p>
                                            {{$application->firstname}} {{$application->lastname}} <br>
                                            <b>E-pasts:</b> <br>
                                            <a href="mailto: {{ $application->email }}">{{ $application->email }}</a> <br>
                                            <b>Tālruņa nr.</b> <br>
                                            <a href="tel: {{ $application->phone }}">{{ $application->phone }}</a> <br>
                                        </p>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-5">
                                    <p><b>Cik bieži tiks uzturēts kontakts ar dzīvnieku un kāpēc?</b></p>
                                    <p>{{ $application->answer1 }}</p>

                                    <p><b>Cik bieži tiks uzturēts kontakts ar dzīvnieku un kāpēc?</b></p>
                                    <p>{{ $application->answer2 }}</p>
                                </div>
                                <div class="col-12 col-lg-5">
                                    <p><b>Dzvnieka uzturēšanās vietas attēls</b></p>
                                    <a href="/storage/{{ $application->picture }}" class="full-image-link">
                                        <img class="image-type-3" src="/storage/{{ $application->picture }} " alt="img">
                                    </a>
                                </div>
                                <div class="col-12">
                                    Status:
                                    @if($application->status == 2)
                                        <b>Noliegts</b>
                                    @endif
                                    @if($application->status == 3)
                                        <b>Apstiprināts</b>
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    @else
                        <p>Nav neviena pieteikuma!</p>
                    @endif
                </div>
            </div>
        </div>
    </section>

@endsection