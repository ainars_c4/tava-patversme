@extends('base')

@section('title', 'Darbinieka pierakstīšanās')

@section('content')

    <section class="centralblockform">
        <div class="contentbox">
            @if(Session::has('success'))
                <div class="succesful">
                    {{Session::get('success')}}
                </div>
            @endif
            <h2 class="box-title">Darbinieka pierakstīšanās</h2>
            <form action="/worker/login" method="post" class="form-box">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="form-group">
                    <label for="Email">Lietotājvārds</label>
                    <input class="tp-input-type-1" required type="text" name="email">
                </div>
                <div class="form-group">
                    <label for="password">Parole</label>
                    <input class="tp-input-type-1" required type="password" name="password">
                </div>
                <div>
                    <input class="tp-btn-type-1" type="submit" value="Pieslēgties">
                </div>
                <div class="errorfield">
                    @foreach($errors->all() as $error)
                        <div>
                            {{$error}}
                        </div>
                    @endforeach
                </div>
            </form>
        </div>
    </section>

@endsection
