@extends('base')

@section('title', 'Darbinieks')

@section('content')

    @include('pages.worker.modules.navbar')

    <section class="base-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2>Darbinieka darba virsma</h2>
                </div>
            </div>
        </div>
    </section>


    <section class="base-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h3>Dzīvnieka pievienošana</h3>
                </div>
                <div class="col-12">
                    <form action="/worker/add-animal" method="post" class="form-box" enctype="multipart/form-data">
                        <div class="successfield">
                            @if (Session::has('add-animal-success'))
                                {{ Session::get('add-animal-success') }}
                            @endif
                        </div>
                        <div class="row">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <label for="name">Vārds</label>
                                    <input class="tp-input-type-1" required type="text" name="name">
                                </div>
                                <div class="form-group">
                                    <label for="Category">Kategorija</label>
                                    <select name="category_id" class="tp-select2-type-1">
                                        @foreach ($categories as $category)
                                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="Desc">Apraksts</label>
                                    <textarea class="tp-textarea-type-1" required type="text" name="desc"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="Birth">Dzimšanas datums</label>
                                    <input class="tp-input-type-1" readonly data-animal-birth required type="text" name="birth">
                                </div>
                                <input type="hidden" name="shelter_id" value="{{ $shelter->id }}">
                            </div>
                            <div class="col-12 col-md-6">
                                <label for="Picture">Bilde</label><br>
                                <div class="box">
                                    <input name="picture" required type="file" id="Picture" class="inputfile inputfile-1" data-parsley-errors-container="#error-1"/>
                                    <label for="Picture"><span>Pievienojiet bildi</span></label>
                                    <div id="error-1"></div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="d-flex justify-content-center">
                                    <input class="tp-btn-type-1" type="submit" value="Pievienot">
                                </div>
                                <div class="errorfield">
                                    @foreach($errors->all() as $error)
                                        <div>
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <section class="base-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h3>Pieejamie dzīvnieki </h3>
                    @if (Session::has('message-adoption-revoked'))
                        <div class="col-12 spacer-sm-top-20">
                            <div class="alert alert-info alert-dismissible fade show" role="alert">
                                {{ Session::get('message-adoption-revoked') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    @endif
                    @if (Session::has('message-delete-animal-success'))
                        <div class="col-12 spacer-sm-top-20">
                            <div class="alert alert-info alert-dismissible fade show" role="alert">
                                {{ Session::get('message-delete-animal-success') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="col-12">
                    @if(count($animals) == 0)
                        <p>Nav neviena dzīvnieka!</p>
                    @endif
                    <div class="animal-table-type-1">
                        @foreach ($animals as $animal)
                            <div class="animal-card">
                                <div class="animal-image">
                                    <img src="/storage/{{ $animal->picture }}" alt="Pic">
                                </div>
                                <div class="animal-name">
                                    {{ $animal->name }}
                                    @if($animal->adopted == 1)
                                        <div>
                                            <b style="color: red">Adoptēts!</b>
                                        </div>
                                        <div>
                                            <a class="btn btn-primary" href="/worker/cancel-adoption/{{ $animal->id }}">Atcelt adoptāciju</a>
                                        </div>
                                    @endif
                                </div>
                                <div class="animal-actions">
                                    <a class="btn btn-primary" href="/worker/edit-animal/{{ $animal->id }}">Rediģēt</a>
                                    <a class="btn btn-primary" href="/worker/delete-animal/{{ $animal->id }}">Dzēst</a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection