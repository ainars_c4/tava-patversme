<section class="worker-navbar">
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">

            <a class="navbar-brand" href="/worker"><b>{{ $shelter->name }}</b></a>
            <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav">
                    <a class="nav-item nav-link @if (Request::is('worker')) active @endif" href="/worker">Sākums</a>
                    @if($worker->admin == 1)
                        <a class="nav-item nav-link @if (Request::is('worker/administration')) active @endif" href="/worker/administration">Administrācija</a>
                    @endif
                    <a class="nav-item nav-link @if (Request::is('worker/applications')) active @endif" href="/worker/applications">Pieteikumi</a>
                </div>

            </div>
    </div>
    </nav>
</section>