@extends('base')

@section('title', 'Patversmes administrators')

@section('content')

    @include('pages.worker.modules.navbar')
    <section class="base-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2>Patversmes administrators</h2>
                </div>
            </div>
        </div>
    </section>

    <section class="base-section">
        <div class="container">
            <div class="row">
                <div class="col-12 spacer-sm-bottom-20">
                    <h3>Patversmes statistika</h3>

                    <div style="height: 300px" class="shelter-statistics-chart">
                        <canvas data-shelter-statistics-chart="{{ $shelter_statistics }}"></canvas>
                    </div>
                </div>
                <div class="col-12 col-md-6 spacer-sm-bottom-30 spacer-md-bottom-0">
                    <h3>Pievienot jaunu darbinieku</h3>
                    <form action="/worker/add-worker" method="post" class="form-box" enctype="multipart/form-data">
                        <div class="row">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <input type="hidden" name="shelter_id" value="{{ $worker->shelter_id }}">
                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    <label for="Firstname">Vārds</label>
                                    <input class="tp-input-type-1" required type="text" name="firstname">
                                </div>
                                <div class="form-group">
                                    <label for="Lastname">Uzvārds</label>
                                    <input class="tp-input-type-1" required type="text" name="lastname">
                                </div>
                                <div class="form-group">
                                    <label for="email">E-pasts</label>
                                    <input class="tp-input-type-1" required type="email" name="email" id="email">
                                </div>
                            </div>

                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    <label for="password">Parole</label>
                                    <input class="tp-input-type-1" required type="password" name="password" id="password">
                                </div>
                                <div class="form-group">
                                    <label for="password_again">Parole (atkal)</label>
                                    <input class="tp-input-type-1" required type="password" name="password" id="password_again" data-parsley-equalto="#password">
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="d-flex justify-content-center spacer-sm-bottom-20">
                                    <input class="tp-btn-type-1" type="submit" value="Pievienot">
                                </div>
                                <div class="errorfield">
                                    @foreach($errors->all() as $error)
                                        <div>
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="successfield">

                                </div>
                                <div class="errorfield">
                                    @if (Session::has('message-email-already-exists'))
                                        <p>{{ Session::get('message-email-already-exists') }}</p>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-12 col-md-6">
                    <h3>Darbību vēsture</h3>
                    <div class="action-history-table-type-1">
                        <div class="action-head">
                            <div class="name">
                                Veicējs
                            </div>
                            <div class="action">
                                Darbība
                            </div>
                            <div class="date">
                                Laiks
                            </div>
                        </div>
                        <div class="history-actions-container">
                            @foreach($action_logs as $action_log)
                                <div class="action-row">
                                    <div class="name">
                                        {{ $action_log->performer }}
                                    </div>
                                    <div class="action">
                                        {{ $action_log->action }}
                                    </div>
                                    <div class="date">
                                        {{ $action_log->created_at }}
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="base-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h3>Patversmes darbinieki</h3>
                </div>
                <div class="col-12">
                    <div class="successfield">
                        @if (Session::has('message-worker-added'))
                            <div class="alert alert-info alert-dismissible fade show" role="alert">
                                {{ Session::get('message-worker-added') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                        @if (Session::has('message-worker-deleted'))
                            <div class="alert alert-info alert-dismissible fade show" role="alert">
                                {{ Session::get('message-worker-deleted') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                        @if (Session::has('message-worker-edited'))
                            <div class="alert alert-info alert-dismissible fade show" role="alert">
                                {{ Session::get('message-worker-edited') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                        <div class="col-12">
                            @if (Session::has('message-password-changed'))
                                <div class="col-12 spacer-sm-top-20">
                                    <div class="alert alert-info alert-dismissible fade show" role="alert">
                                        {{ Session::get('message-password-changed') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="table-type-1">
                        <table>
                            <thead>
                            <tr>
                                <th>Vārds</th>
                                <th>Uzvārds</th>
                                <th>E-pasts</th>
                                <th>Darbības</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($shelter_workers as $shelter_worker)
                                <tr>
                                    <td>{{ $shelter_worker->firstname }}</td>
                                    <td>{{ $shelter_worker->lastname }}</td>
                                    <td>{{ $shelter_worker->email }}</td>
                                    <td>
                                        <a href="/worker/edit-worker/{{  $shelter_worker->id }}" class="btn btn-primary">Rediģet</a>
                                        <a href="/worker/delete-worker/{{  $shelter_worker->id }}" class="btn btn-primary">Dzēst</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>



@endsection