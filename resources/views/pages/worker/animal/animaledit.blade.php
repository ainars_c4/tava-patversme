@extends('base')

@section('title', 'Administrators')

@section('content')

    @include('pages.worker.modules.navbar')

    <section class="base-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <a class="btn-back-type-1 spacer-sm-bottom-20" href="/worker">Atpakaļ</a>
                </div>
                <div class="col-12">
                    <h2>Rediģēt dzīvnieku nr. {{ $animal->id }}</h2>
                </div>
                <div class="col-12">
                    <form action="/worker/edit-animal/{{ $animal->id }}" method="post" class="form-box" enctype="multipart/form-data">
                        <div class="successfield">
                            @if (Session::has('edit-animal-success'))
                                {{ Session::get('edit-animal-success') }}
                            @endif
                        </div>
                        <div class="row">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <label for="Name">Vārds</label>
                                    <input class="tp-input-type-1" required type="text" name="name" value="{{ $animal->name }}">
                                </div>
                                <div class="form-group">
                                    <label for="Category">Kategorija</label>
                                    {{--<input class="tp-input-type-1" required type="text" name="Name">--}}
                                    <select name="category_id" class="tp-select2-type-1">
                                        @foreach ($categories as $category)
                                            <option @if($animal->category_id == $category->id) selected @endif value="{{ $category->id }}">{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="Desc">Apraksts</label>
                                    <textarea class="tp-textarea-type-1" required type="text" name="desc">{{ $animal->desc }}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="Birth">Dzimšanas datums</label>
                                    <input class="tp-input-type-1" data-animal-birth required type="text" name="birth" value="{{ $animal->birth }}">
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="file-type-1" data-file-open="1">
                                    <div class="row">
                                        <div class="col-12">
                                            <img class="image-type-1" src="/storage/{{ $animal->picture }}" alt="Pic">
                                        </div>
                                        <div class="col-12 spacer-sm-bottom-30 spacer-md-bottom-0">
                                            <button type="button" class="btn btn-primary" data-change-file>Mainīt</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group" data-file-closed="1">
                                    <label for="Picture">Jaunā bilde</label>
                                    <div class="box">
                                        <input name="picture" type="file" id="Picture" class="inputfile inputfile-1" data-parsley-errors-container="#error-1" data-add-required/>
                                        <label for="Picture"><span>Pievienojiet bildi</span></label>
                                        <div id="error-1"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="d-flex justify-content-center">
                                    <input class="tp-btn-type-1" type="submit" value="Saglabāt">
                                </div>
                                <div class="errorfield">
                                    @foreach($errors->all() as $error)
                                        <div>
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <section class="base-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2>Dzīvnieka slimības</h2>
                </div>
                <div class="col-12">
                    <div class="successfield">
                        @if (Session::has('edit-animal-sickness-success'))
                            <p>{{ Session::get('edit-animal-sickness-success') }}</p>
                        @endif
                        @if (Session::has('delete-animal-sickness-success'))
                            <p>{{ Session::get('delete-animal-sickness-success') }}</p>
                        @endif
                    </div>
                    @if(count($animal_diseases) == 0)
                        <p>Dzīvniekam nav slimību.</p>
                    @else
                        <ul class="card-holder">
                            @foreach($animal_diseases as $animal_disease)
                                <li>
                                    <div class="animal-sickness-card spacer-sm-bottom-30">
                                        <div class="row">
                                            <div class="col-sm-12 col-md-4">
                                                <div>Slimība: </div>
                                                <div class="spacer-sm-bottom-20"><b>{{ $animal_disease->name }}</b></div>
                                            </div>
                                            <div class="col-sm-4 col-md-4">
                                                <div>Diagnozes datums:</div>
                                                <div class="spacer-sm-bottom-20 spacer-md-bottom-0"><b>{{ $animal_disease->begindate }}</b></div>
                                            </div>
                                            <div class="col-sm-12 col-md-4 spacer-sm-bottom-20 spacer-md-bottom-0">
                                                <div class="spacer-sm-bottom-5">Darbības:</div>
                                                <a class="btn btn-primary" href="/worker/edit-animal-sickness/{{ $animal_disease->animalid }}/{{ $animal_disease->sicknessid }}/{{ $animal_disease->begindate }}">Rediģet</a>
                                                <a class="btn btn-primary" href="/worker/delete-animal-sickness/{{ $animal_disease->animalid }}/{{ $animal_disease->sicknessid }}/{{ $animal_disease->begindate }}">Dzēst</a>
                                            </div>
                                            <div class="col-sm-12 col-md-8">
                                                <div>Apraksts: </div>
                                                <div class="description spacer-sm-bottom-20">
                                                    {{ $animal_disease->desc }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    @endif

                    <div>
                        <button class="tp-btn-type-1 w-auto" data-show="add-sickness-window">
                            <i class="add-green-circle"></i>Pievienot jaunu slimību
                        </button>
                    </div>

                    <div class="appear-card-type-1" id="add-sickness-window">
                        <div class="successfield">
                            @if (Session::has('add-sickness-success'))
                                {{ Session::get('add-sickness-success') }}
                            @endif
                        </div>
                        <div class="errorfield">
                            @if (Session::has('add-sickness-failed'))
                                {{ Session::get('add-sickness-failed') }}
                            @endif
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <h3>Jaunas slimības pievienošana</h3>
                            </div>
                            <div class="col-12">
                                <form action="/worker/add-existing-sickness/{{$animal->id}}" method="post" class="form-box" enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-12 col-md-6">
                                            <div class="form-group">
                                                <div>Izvēlaties slimību:</div>
                                                <select required name="sicknessid" class="tp-select2-type-1">
                                                    @foreach($diseases as $disease)
                                                        <option value="{{ $disease->id }}">{{ $disease->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-6">
                                            <div class="form-group">
                                                <div>Diagnozes datums:</div>
                                                <input type="text" readonly data-sickness-date class="tp-input-type-1" name="begindate">
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-6">
                                            <div class="form-group">
                                                <div>Apraksts:</div>
                                                <textarea name="desc" class="tp-textarea-type-1"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">

                                    <button type="submit" class="btn btn-primary">Pievienot</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection