@extends('base')

@section('title', 'Administrators')

@section('content')

    @include('pages.worker.modules.navbar')

    <section class="base-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <a class="btn-back-type-1 spacer-sm-bottom-20" href="/worker/edit-animal/{{$animal_disease->animalid}}">Atpakaļ</a>
                </div>
                <div class="col-12">
                    <h2>Dzīvnieka slimības rediģēšana</h2>
                </div>
                <div class="col-12">
                    <form action="" method="post" class="form-box" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div>Apraksts:</div>
                        <textarea name="desc" class="tp-textarea-type-1">{{$animal_disease->desc}}</textarea>

                        <button type="submit" class="btn btn-primary">Saglabāt</button>
                    </form>
                </div>
            </div>
        </div>
    </section>

@endsection