@extends('base')

@section('title', 'Darbinieks')

@section('content')

    @include('pages.worker.modules.navbar')

    <section class="base-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <a class="btn-back-type-1 spacer-sm-bottom-20" href="/worker/administration">Atpakaļ</a>
                </div>
                <div class="col-12 spacer-sm-bottom-20">
                    <h2>Darbinieka rediģēšana</h2>
                </div>
                <div class="col-12">
                    <h3>Mainīt datus</h3>
                </div>
                <div class="col-12 spacer-sm-bottom-20">
                    <form action="/worker/edit-worker/{{$shelter_worker->id}}" method="post" class="form-box" enctype="multipart/form-data">
                        <div class="row">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <input type="hidden" name="Shelter_ID" value="{{ $worker->Shelter_ID }}">
                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    <label for="Firstname">Vārds</label>
                                    <input class="tp-input-type-1" required type="text" name="firstname" value="{{$shelter_worker->firstname}}">
                                </div>
                                <div class="form-group">
                                    <label for="Lastname">Uzvārds</label>
                                    <input class="tp-input-type-1" required type="text" name="lastname" value="{{$shelter_worker->lastname}}">
                                </div>
                            </div>

                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    <label for="Lastname">E-pasts</label>
                                    <input class="tp-input-type-1" required type="text" name="email" value="{{$shelter_worker->email}}">
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="d-flex justify-content-center">
                                    <input class="tp-btn-type-1" type="submit" value="Saglabāt">
                                </div>
                                <div class="errorfield">
                                    @foreach($errors->all() as $error)
                                        <div>
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="errorfield">
                                    @if (Session::has('message-email-already-exists'))
                                        <p>{{ Session::get('message-email-already-exists') }}</p>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="col-12">
                    <h3>Mainīt paroli</h3>
                </div>

                <div class="col-12">
                    <form action="/worker/edit-worker-password/{{$shelter_worker->id}}" method="post" class="form-box" enctype="multipart/form-data">
                        <div class="row">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <input type="hidden" name="Shelter_ID" value="{{ $worker->Shelter_ID }}">
                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    <label for="password">Parole</label>
                                    <input class="tp-input-type-1" required type="password" name="password" id="password">
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    <label for="password_again">Parole atkārtoti</label>
                                    <input class="tp-input-type-1" required type="password" id="password_again" data-parsley-equalto="#password">
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="d-flex justify-content-center">
                                    <input class="tp-btn-type-1" type="submit" value="Mainīt">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>


@endsection