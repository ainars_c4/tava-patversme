@extends('base')

@section('title', 'Mans profils')

@section('content')

    @include('pages.worker.modules.navbar')

    <section class="base-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <a href="/worker" class="btn-back-type-1 spacer-sm-bottom-20">Atpakaļ</a>
                </div>
                <div class="col-12">
                    <h2>Mans profils</h2>
                </div>
            </div>
        </div>
    </section>

    <section class="base-section">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6 spacer-sm-bottom-30">
                    <h3>Mani dati</h3>

                    <div class="errorfield spacer-sm-bottom-20 spacer-sm-t">
                        @foreach($errors->all() as $error)
                            <div>
                                {{$error}}
                            </div>
                        @endforeach
                    </div>

                    <form action="/worker/edit-me" method="post" class="form-box" enctype="multipart/form-data">
                        <div class="row">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <input type="hidden" name="shelter_id" value="{{ $worker->shelter_id }}">
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="firstname">Vārds</label>
                                    <input class="tp-input-type-1" required type="text" name="firstname" value="{{$worker->firstname}}">
                                </div>
                                <div class="form-group">
                                    <label for="lastname">Uzvārds</label>
                                    <input class="tp-input-type-1" required type="text" name="lastname" value="{{$worker->lastname}}">
                                </div>
                            </div>
                            <div class="col-12">
                                <input class="tp-btn-type-1" type="submit" value="Saglabāt">
                            </div>

                            @if (Session::has('message-edit-successful'))
                                <div class="col-12 spacer-sm-top-20">
                                    <div class="alert alert-info alert-dismissible fade show" role="alert">
                                        {{ Session::get('message-edit-successful') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </form>
                </div>
                <div class="col-12 col-md-6">
                    <h3>Manīt paroli</h3>

                    <form action="/worker/edit-my-password" method="post" class="form-box" enctype="multipart/form-data">
                        <div class="row">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <input type="hidden" name="shelter_id" value="{{ $worker->shelter_id }}">
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="password">Parole</label>
                                    <input class="tp-input-type-1" required type="password" name="password" id="password">
                                </div>
                                <div class="form-group">
                                    <label for="password_again">Parole atkārtoti</label>
                                    <input class="tp-input-type-1" required id="password_again" type="password" data-parsley-equalto="#password">
                                </div>
                            </div>
                            <div class="col-12">
                                <input class="tp-btn-type-1" type="submit" value="Mainīt">
                            </div>

                            @if (Session::has('message-edit-password-successful'))
                                <div class="col-12 spacer-sm-top-20">
                                    <div class="alert alert-info alert-dismissible fade show" role="alert">
                                        {{ Session::get('message-edit-password-successful') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>


@endsection