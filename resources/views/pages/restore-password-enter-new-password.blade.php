@extends('base')

@section('title', 'Pierakstīšanās')

@section('content')

    <section class="centralblockform">
        <div class="contentbox">
            @if(Session::has('error-no-user-found'))
                <h1>Sistēmas kļūda!</h1>
                <p>Lūdzu mēģinat vēlreiz iziet cauri paroles atjaunošanas procesam.</p>
            @else
                <h2 class="box-title">Paroles atjaunošana</h2>

                <form action="/restore-password/new-password" method="post" class="form-box">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="token" value="{{$token}}">
                    <div class="form-group">
                        <label for="password">Parole</label>
                        <input class="tp-input-type-1" required type="password" name="password" id="password">
                    </div>
                    <div class="form-group">
                        <label for="password_again">Parole atkārtoti</label>
                        <input class="tp-input-type-1" required type="password" id="password_again" data-parsley-equalto="#password">
                    </div>
                    <div class="errorfield">
                        @foreach($errors->all() as $error)
                            <div>
                                {{$error}}
                            </div>
                        @endforeach
                    </div>
                    <div>
                        <input class="tp-btn-type-1" type="submit" value="Atjaunot paroli">
                    </div>
                </form>
            @endif
        </div>
    </section>
@endsection
