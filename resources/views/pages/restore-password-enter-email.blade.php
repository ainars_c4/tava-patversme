@extends('base')

@section('title', 'Pierakstīšanās')

@section('content')

    <section class="centralblockform">
        <div class="contentbox">
            @if(Session::has('success'))
                <div class="succesful">
                    {{Session::get('success')}}
                </div>
            @endif
            <h2 class="box-title">Paroles atjaunošana</h2>
            <p>
                <b>Aizmirsāt paroli?</b>
                <br>
                Tā nav problēma!
                <br>
                Lūdzu ievadiet savu e-pasta adresi, un jūs varēsiet
                <br>
                savam profilam izveidot jaunu paroli!
            </p>
            <form action="/restore-password" method="post" class="form-box">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="form-group">
                    <label for="Email">E-pasts</label>
                    <input class="tp-input-type-1" required type="email" name="email">
                </div>
                <div class="errorfield">
                    @foreach($errors->all() as $error)
                        <div>
                            {{$error}}
                        </div>
                    @endforeach
                </div>
                <div>
                    <input class="tp-btn-type-1" type="submit" value="Atjaunot paroli">
                </div>
            </form>
        </div>
    </section>
@endsection
