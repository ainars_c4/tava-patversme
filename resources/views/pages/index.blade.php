@extends('base')

@section('title', 'Galvenā lapa')

@section('content')
    <section class="base-section">
        <div class="container">
            @if(Session::has('message-email-verified'))
                <div class="col-12">
                    <div class="message-field">
                        <div class="default">
                            {{Session::get('message-email-verified')}}
                        </div>
                    </div>
                </div>
            @endif
            @if(!Auth::user())
                <div class="col-12 notice">
                    <div class="alert alert-info alert-dismissible fade show" role="alert">
                        Lai varētu adoptēt dzīvniekus, ir nepieciešams reģistrēties.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            @endif
            <div class="col-12 notice">
                @if (Session::has('message-password-change-success'))
                    <div class="col-12 spacer-sm-top-20">
                        <div class="alert alert-info alert-dismissible fade show" role="alert">
                            {{ Session::get('message-password-change-success') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </section>
    <section class="base-section">
        <div class="container">
            <div class="container-type-1">
                <div class="sidebar">
                    <div class="sidebar-title">Kategorijas</div>
                    <ul>
                        @foreach($categories as $category)
                            <li><a href="/category/{{$category->name}}">{{$category->name}}</a></li>
                        @endforeach
                    </ul>
                </div>
                <div class="content">
                    <h2>Pieejamie dzīvnieki</h2>
                    <div class="animal-table-type-2">
                        @foreach ($animals as $animal)
                            <div class="animal-card">
                                <div class="animal-image">
                                    <img src="/storage/{{ $animal->picture }}" alt="Pic">
                                </div>
                                <div class="animal-name">
                                    {{ $animal->name }}
                                </div>
                                <div class="animal-actions">
                                    <a class="btn btn-primary" href="/view-animal/{{ $animal->id }}">Apskatīt</a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
