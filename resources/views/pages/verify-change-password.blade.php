@extends('base')

@section('title', 'Pierakstīšanās')

@section('content')

    <section class="centralblockform">
        <div class="contentbox">
            <h2 class="box-title">Paroles maiņa</h2>
            <p>
                Lai pabeigtu paroles maiņu, lūdzu dodaties uz savu epastu un sekojiet mūsu ziņas instrukcijām.
            </p>
        </div>
    </section>
@endsection
