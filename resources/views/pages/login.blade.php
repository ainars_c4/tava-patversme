@extends('base')

@section('title', 'Pierakstīšanās')

@section('content')

    <section class="centralblockform">
        <div class="contentbox">
            @if(Session::has('success'))
                <div class="succesful">
                    {{Session::get('success')}}
                </div>
            @endif
            @if (Session::has('message-password-reset-success'))
                <div class="col-12 spacer-sm-top-20">
                    <div class="alert alert-info alert-dismissible fade show" role="alert">
                        {{ Session::get('message-password-reset-success') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            @endif
            <h2 class="box-title">Pierakstīšanās</h2>
            <form action="" method="post" class="form-box">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="form-group">
                    <label for="Email">E-pasts</label>
                    <input class="tp-input-type-1" required type="email" name="email">
                </div>
                <div class="form-group">
                    <label for="password">Parole</label>
                    <input class="tp-input-type-1" required type="password" name="password">
                </div>
                <div>
                    <input class="tp-btn-type-1" type="submit" value="Pieslēgties">
                </div>
                <div class="errorfield">
                    @foreach($errors->all() as $error)
                        <div>
                            {{$error}}
                        </div>
                    @endforeach
                </div>

                <div class="col-12 spacer-sm-top-20 text-center">
                    <a href="/forgot-password">Aizmirsi paroli?</a>
                </div>
            </form>
        </div>
    </section>
@endsection
