@extends('base')

@section('title', 'Galvenā lapa')

@section('content')

    <section class="base-section">
        <div class="container">
            <div class="row">
                <div class="col-12 spacer-sm-bottom-30">
                    <a href="/" class="btn-back-type-1">Atpakaļ</a>
                </div>
                <div class="col-12">
                    <h3>{{$animal->name}}</h3>
                </div>
            </div>
        </div>
    </section>
    <section class="base-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="col-12 col-md-6 order-md-2">
                            <img class="image-type-2" src="/storage/{{$animal->picture}}" alt="img">
                        </div>
                        <div class="col-12 col-md-6 order-md-1">
                            <p><b>Dzīvnieka apraksts</b></p>
                            <p>{{$animal->desc}}</p>
                            <p><b>Vecums</b>: {{ \Carbon\Carbon::parse($animal->birth)->diff(\Carbon\Carbon::now())->format('%y gadi un %m mēneši') }}</p>
                            <br>
                            @if(Auth::guard('web')->check())
                                @if($has_applied)
                                    <p>Jūs esat pieprasījis šo dzīvnieku!</p>
                                @else
                                    <a href="/adopt-animal/{{$animal->id}}" class="btn btn-primary">Adoptēt</a>
                                @endif
                            @endif
                        </div>
                        <div class="col-12 col-md-6 order-md-3">
                            <h4>Dzīvnieka veselības vēsture:</h4>
                            @if(Count($health_history))
                                @foreach($health_history as $health)
                                    <div class="disease-table-type-1 spacer-sm-bottom-20">
                                        <div class="part-1">
                                            <div class="title">Slimība</div>
                                            <div class="disease">{{ $health->name }}</div>
                                        </div>
                                        <div class="part-2">
                                            <div class="title">Apraksts</div>
                                            <div class="description">
                                                {{ $health->desc }}
                                            </div>
                                        </div>
                                        <div class="part-3">
                                            <div class="diagnosis">
                                                Diagnozes datums: <b>{{ $health->begindate }}</b>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                <p>Dzīvnieks nekad nav slimojis!</p>
                            @endif
                        </div>
                        <div class="col-12 col-md-6 order-md-4">
                            <div class="shelter-box-type-1">
                                <h4>Patvermse</h4>

                                <div class="shelter-name">
                                    {{$animal->sheltername}}
                                </div>
                                <div class="shelter-adress">
                                    {{$animal->city}}, {{$animal->street}} {{$animal->street_nr}}
                                </div>
                                <div class="shelter-img">
                                    <img class="image-type-3" src="/storage/{{$animal->shelterpic}}" alt="img">
                                </div>
                                <br>
                                <br>
                                <div>
                                    Kontaktinformācija:
                                    @foreach($contactinfo as $contact)
                                        <br>
                                        @if($contact->email) E-pasts : <a href="mailto: {{ $contact->email }}">{{ $contact->email }}</a> @endif
                                        <br>
                                        @if($contact->phone) Tālruņa nr. : <a href="tel: {{ $contact->phone }}">{{ $contact->phone }}</a> @endif
                                        <br><br>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection