@extends('base')

@section('title', 'Reģistrācija')

@section('content')

    <section class="base-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1 class="box-title">Reģistrācija</h1>
                </div>
                <div class="col-12">
                    <form action="/register" method="post" class="form-box">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <div class="form-group">
                                    <label for="firstname">Vārds</label>
                                    <input class="tp-input-type-1" required type="text" name="firstname">
                                </div>
                                <div class="form-group">
                                    <label for="lastname">Uzvārds</label>
                                    <input class="tp-input-type-1" required type="text" name="lastname">
                                </div>
                                <div class="form-group">
                                    <label for="phone">Tālruņa nr.</label>
                                    <input class="tp-input-type-1" required type="text" name="phone">
                                </div>
                                <div class="form-group">
                                    <label for="city">Pilsēta</label>
                                    <input class="tp-input-type-1" required type="text" name="city">
                                </div>
                                <div class="form-group">
                                    <label for="street">Iela</label>
                                    <input class="tp-input-type-1" required type="text" name="street">
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    <label for="street_nr">Ielas nr.</label>
                                    <input class="tp-input-type-1" type="text" name="street_nr">
                                </div>
                                <div class="form-group">
                                    <label for="password">Parole</label>
                                    <input class="tp-input-type-1" required type="password" name="password">
                                </div>
                                <div class="form-group">
                                    <label for="email">E-pasts</label>
                                    <input class="tp-input-type-1" required type="email" name="email">
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="d-flex justify-content-center">
                                    <input class="tp-btn-type-1" type="submit" value="Reģistrēties">
                                </div>
                                <div class="errorfield">
                                    @if(Session::has('message-email-exists'))
                                        <div class="error">
                                            {{Session::get('message-email-exists')}}
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

@endsection
