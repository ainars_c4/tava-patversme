<?php
////Admin routes
//Route::prefix('admin')->group(function() {
//
//    //Admin authentication
//    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
//
//    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
//
//    Route::get('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');
//
//    Route::get('/dashboard', 'AdminController@getAdminPage');
//
//    Route::get('/', 'AdminController@getAdminPage');
//
//    //Shelter routes
//    Route::post('/add-shelter', 'ShelterController@addShelter');
//
//    Route::get('/delete-shelter/{id}', 'ShelterController@removeShelter');
//
//    Route::get('/edit-shelter/{id}', 'ShelterController@showEditShelterForm');
//
//    Route::post('/edit-shelter/{id}', 'ShelterController@editShelter');
//
//    Route::post('/add-contact-info/{id}', 'ShelterController@addContactFromForm');
//
//    Route::get('/delete-contact-info/{id}', 'ShelterController@deleteContact');
//
//    Route::get('/edit-contact-info/{id}', 'ShelterController@getEditContactPage');
//
//    Route::post('/edit-contact-info/{id}', 'ShelterController@editContact');
//
//    Route::post('/add-category', 'AnimalController@addCategory');
//
//    Route::get('/delete-category/{id}', 'AnimalController@deleteCategory');
//
//    Route::get('/edit-category/{id}', 'AnimalController@getEditCategoryPage');
//
//    Route::post('/edit-category/{id}', 'AnimalController@editCategory');
//});

//Worker routes
Route::prefix('worker')->group(function() {

    //Navigation
    Route::get('/login', 'Auth\WorkerLoginController@showLoginForm')->name('worker.login');

    Route::post('/login', 'Auth\WorkerLoginController@login')->name('worker.login.submit');

    Route::get('/logout', 'Auth\WorkerLoginController@logout');

    Route::get('/', 'WorkerController@getWorkerPage');

    //Worker function routes
    Route::get('/my-profile', 'WorkerController@getMyProfilePage');

    Route::post('/edit-me', 'WorkerController@editMe');

    Route::post('/edit-my-password', 'WorkerController@editMyPassword');

    Route::post('/add-worker', 'WorkerController@addWorker');

    Route::get('/delete-worker/{id}', 'WorkerController@deleteWorker');

    Route::get('/edit-worker/{id}', 'WorkerController@getEditWorkerPage');

    Route::post('/edit-worker/{id}', 'WorkerController@editWorker');

    Route::post('/edit-worker-password/{id}', 'WorkerController@editWorkerPassword');

    Route::get('/applications', 'WorkerController@getApplicationsPage')->name('worker.dashboard');

    Route::get('/applications/accept/{ApplicationID}', 'WorkerController@acceptApplication');

    Route::get('/applications/deny/{ApplicationID}', 'WorkerController@rejectApplication');

    //Animal function routes
    Route::get('/administration', 'WorkerController@getWorkerAdminPage');

    Route::post('/add-animal', 'AnimalController@createAnimal');

    Route::get('/edit-animal/{id}', 'AnimalController@getEditAnimalView');

    Route::post('/edit-animal/{id}','AnimalController@editAnimal' );

    Route::get('/delete-animal/{id}','AnimalController@deleteAnimal' );

    Route::post('/add-sickness/{id}','AnimalController@addNewSickness' );

    Route::post('/add-existing-sickness/{id}','AnimalController@addNewExistingSickness' );

    Route::get('/edit-animal-sickness/{AnimalID}/{SicknessID}/{BeginDate}', 'AnimalController@getEditAnimalSicknessForm');

    Route::post('/edit-animal-sickness/{AnimalID}/{SicknessID}/{BeginDate}', 'AnimalController@editAnimalSickness');

    Route::get('/delete-animal-sickness/{AnimalID}/{SicknessID}/{BeginDate}', 'AnimalController@deleteAnimalSickness');

    Route::get('/cancel-adoption/{id}', 'AnimalController@cancelAdoption');
});

//User routes

Route::get('/', 'UserController@getIndex');

Route::get('/forgot-password', 'UserController@getRestorePasswordPage');

Route::post('/restore-password', 'UserController@restorePassword');

Route::get('/restore-password', 'UserController@getEnterNewPasswordPage')->name('auth.verify_reset_password');

Route::post('/restore-password/new-password', 'UserController@setNewPassword');

Route::get('/show-verify-email-message', 'Auth\RegisterController@getVerifyEmailPage')->name('auth.show_verify_email');

Route::get('/show-verify-deletion', 'UserController@getVerifyDeletionPage')->name('auth.show_verify_deletion');

Route::get('/show-change-password', 'UserController@getVerifyChangePasswordPage')->name('auth.show_verify_change_password');

Route::get('/verify-email', 'Auth\RegisterController@verifyEmail')->name('auth.verify_email');

Route::get('/verify-deletion', 'UserController@verifyDeletion')->name('auth.verify_deletion');

Route::get('/verify-change-password', 'UserController@verifyChangePassword')->name('auth.verify_change_password');

Route::get('/category/{category}', 'UserController@getCategoryPage');

Route::get('/login', 'Auth\LoginController@getLoginPage');

Route::post('/login', 'Auth\LoginController@login');

Route::get('/logout', 'UserController@logout');

Route::get('/profile', 'UserController@getEditPage');

Route::post('/edit-user', 'UserController@edit');

Route::post('/edit-password', 'UserController@changePassword');

Route::get('/delete-user', 'UserController@delete');

Route::get('/register', 'Auth\RegisterController@getRegisterPage');

Route::post('/register', 'Auth\RegisterController@registerUser');

Route::get('/view-animal/{id}', 'AnimalController@getViewAnimalPage');

Route::get('/adopt-animal/{id}', 'AnimalController@getAdoptPage');

Route::post('/make-application/{id}', 'AnimalController@makeApplication');

//Api routes

Route::get('/api/get-shelter-names', 'ShelterController@getShelterNames');

Route::get('/api/get-shelter-worker-ids', 'ShelterController@getShelterWorkerIds');

Route::get('/api/get-disease-names', 'DiseaseController@getDiseaseNames');

Route::get('/api/get-animal-names', 'AnimalController@getAnimalNames');