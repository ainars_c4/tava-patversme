How to start up:

copy .env.example to .env in the same dir (/app)

composer install

php artisan migrate --force

php artisan key:generate

npm install

npm i parsleyjs

php artisan storage:link

npm run production   (npm run dev   OR   npm run watch)-> for developers