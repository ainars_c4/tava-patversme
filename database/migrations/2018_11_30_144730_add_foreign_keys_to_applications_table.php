<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToApplicationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('applications', function(Blueprint $table)
		{
			$table->foreign('animalid', 'fk_Applications_Animals1')->references('id')->on('animals')->onUpdate('NO ACTION')->onDelete('CASCADE');
			$table->foreign('userid', 'fk_Applications_Users1')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('applications', function(Blueprint $table)
		{
			$table->dropForeign('fk_Applications_Animals1');
			$table->dropForeign('fk_Applications_Users1');
		});
	}

}
