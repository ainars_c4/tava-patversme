<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToAnimalsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('animals', function(Blueprint $table)
		{
			$table->foreign('category_id', 'fk_Animals_Categories1')->references('id')->on('categories')->onUpdate('NO ACTION')->onDelete('CASCADE');
			$table->foreign('shelter_id', 'fk_Animals_Shelt1')->references('id')->on('shelters')->onUpdate('NO ACTION')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('animals', function(Blueprint $table)
		{
			$table->dropForeign('fk_Animals_Categories1');
			$table->dropForeign('fk_Animals_Shelt1');
		});
	}

}
