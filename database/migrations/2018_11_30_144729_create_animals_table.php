<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAnimalsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('animals', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 45);
			$table->integer('category_id')->unsigned()->index('fk_Animals_Categories1_idx');
			$table->text('desc', 10000)->nullable();
			$table->string('picture', 45)->nullable();
			$table->integer('shelter_id')->unsigned()->index('fk_Animals_Shel');
			$table->date('birth')->nullable();
            $table->tinyInteger('adopted')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('animals');
	}

}
