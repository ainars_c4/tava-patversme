<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToContactInfoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('contact_info', function(Blueprint $table)
		{
			$table->foreign('shelter_id', 'fk_Contact_info_Shelters')->references('id')->on('shelters')->onUpdate('NO ACTION')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('contact_info', function(Blueprint $table)
		{
			$table->dropForeign('fk_Contact_info_Shelters');
		});
	}

}
