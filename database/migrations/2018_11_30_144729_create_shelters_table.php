<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSheltersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('shelters', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 45);
			$table->string('city', 25);
			$table->string('street', 50);
			$table->string('street_nr', 20)->nullable();
			$table->string('picture', 100)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('shelters');
	}

}
