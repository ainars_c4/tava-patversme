<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToAnimalHealthHistoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('animal_health_history', function(Blueprint $table)
		{
			$table->foreign('animalid', 'fk_Animal_health_history_Animals1')->references('id')->on('animals')->onUpdate('NO ACTION')->onDelete('CASCADE');
			$table->foreign('sicknessid', 'fk_Animal_health_history_Diseases1')->references('id')->on('diseases')->onUpdate('CASCADE')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('animal_health_history', function(Blueprint $table)
		{
			$table->dropForeign('fk_Animal_health_history_Animals1');
			$table->dropForeign('fk_Animal_health_history_Diseases1');
		});
	}

}
