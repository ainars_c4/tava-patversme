<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateApplicationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('applications', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('userid')->unsigned()->index('fk_Applications_Users1_idx');
			$table->integer('animalid')->unsigned()->index('fk_Applications_Animals1_idx');
			$table->text('answer1', 10000);
			$table->text('answer2', 10000);
			$table->string('picture', 45)->nullable();
			$table->string('status', 1)->nullable()->default('3');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('applications');
	}

}
