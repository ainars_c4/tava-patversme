<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAnimalHealthHistoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('animal_health_history', function(Blueprint $table)
		{
            $table->increments('id');
			$table->integer('animalid')->unsigned();
			$table->integer('sicknessid')->unsigned()->index('fk_Animal_health_history_Diseases1_idx');
			$table->text('desc', 65535)->nullable();
			$table->date('begindate');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('animal_health_history');
	}

}
