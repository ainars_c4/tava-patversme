<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToShelterWorkersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('shelter_workers', function(Blueprint $table)
		{
			$table->foreign('shelter_id', 'fk_Shelter_workers_Shelters1')->references('id')->on('shelters')->onUpdate('NO ACTION')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('shelter_workers', function(Blueprint $table)
		{
			$table->dropForeign('fk_Shelter_workers_Shelters1');
		});
	}

}
