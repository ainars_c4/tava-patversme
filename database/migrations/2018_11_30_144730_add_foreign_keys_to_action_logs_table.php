<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToActionLogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('action_logs', function(Blueprint $table)
		{
            $table->foreign('related_shelter_id', 'fk_action_log_related_shelter_id_idx')->references('id')->on('shelters')->onUpdate('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('action_logs', function(Blueprint $table)
		{
            $table->dropForeign('fk_action_log_related_shelter_id_idx');
		});
	}

}
