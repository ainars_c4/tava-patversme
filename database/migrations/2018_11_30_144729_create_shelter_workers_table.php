<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateShelterWorkersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('shelter_workers', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('firstname', 32);
			$table->string('lastname', 32);
			$table->integer('shelter_id')->unsigned()->index('fk_Shelter_workers_Shelters1_idx');
			$table->string('password', 200);
			$table->string('email', 45)->unique();
			$table->boolean('admin')->default(0);
			$table->string('remember_token', 100)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('shelter_workers');
	}

}
