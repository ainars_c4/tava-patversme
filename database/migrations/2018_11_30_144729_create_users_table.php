<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('firstname', 32);
			$table->string('lastname', 40);
			$table->string('phone', 45);
			$table->string('city', 25);
			$table->string('street', 50);
			$table->string('street_nr', 20)->nullable();
			$table->string('email', 45)->unique();
			$table->string('password', 100);
			$table->dateTime('email_verified_at')->nullable();
			$table->string('remember_token', 100)->nullable();
            $table->string('token', 60)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
