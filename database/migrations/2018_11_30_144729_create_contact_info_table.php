<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContactInfoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contact_info', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('shelter_id')->unsigned()->index('fk_Contact_info_Shelters_idx');
			$table->string('email', 45)->nullable();
			$table->string('phone', 45)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contact_info');
	}

}
