<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateActionLogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('action_logs', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('code');
			$table->longText('action', 10000);
			$table->string('performer', 150);
			$table->integer('related_shelter_id')->unsigned()->nullable()->index('fk_action_log_related_shelter_id_idx');
            $table->datetime('created_at');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('action_logs');
	}

}
